<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Penggajian_model extends CI_Model
{

    public $table = 'pos_penggajian';
    public $id = 'id';
    public $order = 'DESC';

    function __construct()
    {
        parent::__construct();
    }

    // get all
    function get_all()
    {
        $this->db->order_by($this->id, $this->order);
        return $this->db->get($this->table)->result();
    }

    public function getPegawai()
    {

        $this->db->select('a.*, b.gaji_pokok, b.tunjangan,b.bonus');
        $this->db->from("dat_pegawai a");
        $this->db->join('dat_gaji b', 'a.nip = b.nip'); // this joins the user table to topics
        return $this->db->get()->result();
    }

    // get data by id
    function get_by_id($id)
    {
        $this->db->where($this->id, $id);
        return $this->db->get($this->table)->row();
    }

    // get total rows
    function total_rows($q = NULL) {
        $this->db->like('id', $q);
	$this->db->or_like('tanggal_transaksi', $q);
	$this->db->or_like('bulan_gaji', $q);
	$this->db->or_like('tahun_gaji', $q);
	$this->db->or_like('nip_pegawai', $q);
	$this->db->or_like('nama_pegawai', $q);
	$this->db->or_like('gaji_pokok', $q);
	$this->db->or_like('tunjangan', $q);
	$this->db->or_like('bonus', $q);
	$this->db->or_like('date_insert', $q);
	$this->db->or_like('nip_insert', $q);
	$this->db->from($this->table);
        return $this->db->count_all_results();
    }

    // get data with limit and search
    function get_limit_data($limit, $start = 0, $q = NULL) {
        $this->db->order_by($this->id, $this->order);
        $this->db->like('id', $q);
	$this->db->or_like('tanggal_transaksi', $q);
	$this->db->or_like('bulan_gaji', $q);
	$this->db->or_like('tahun_gaji', $q);
	$this->db->or_like('nip_pegawai', $q);
	$this->db->or_like('nama_pegawai', $q);
	$this->db->or_like('gaji_pokok', $q);
	$this->db->or_like('tunjangan', $q);
	$this->db->or_like('bonus', $q);
	$this->db->or_like('date_insert', $q);
	$this->db->or_like('nip_insert', $q);
	$this->db->limit($limit, $start);
        return $this->db->get($this->table)->result();
    }

    // insert data
    function insert($data)
    {
        $this->db->insert($this->table, $data);
    }

    // update data
    function update($id, $data)
    {
        $this->db->where($this->id, $id);
        $this->db->update($this->table, $data);
    }

    // delete data
    function delete($id)
    {
        $this->db->where($this->id, $id);
        $this->db->delete($this->table);
    }


    // get total rows
    function laporan_total_rows($q = NULL) {
        $this->db->from('pos_penggajian');
            return $this->db->count_all_results();
        }

    // get data with limit and search
    function laporan_get_limit_data($limit, $start = 0, $q = NULL) {
        $this->db->limit($limit, $start);
            return $this->db->get('pos_penggajian')->result();
        }
        function getAll_data() {
                return $this->db->get('pos_penggajian')->result();
            }
}

/* End of file Penggajian_model.php */
/* Location: ./application/models/Penggajian_model.php */