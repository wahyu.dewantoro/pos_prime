<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Pembelian_model extends CI_Model
{

    public $table = 'pos_pembelian';
    public $id = 'id';
    public $order = 'ASC';

    function __construct()
    {
        parent::__construct();
    }

    // get all
    function get_all()
    {
        $this->db->order_by($this->id, $this->order);
        return $this->db->get($this->table)->result();
    }

    // get data by id
    function get_by_id($id)
    {
        $this->db->where($this->id, $id);
        return $this->db->get($this->table)->row();
    }

    // get total rows
    function total_rows($q = NULL) {
    $this->db->or_like('tgl_pembelian', $q);
    $this->db->or_like('nama_suplier', $q);
    $this->db->or_like('nip_pegawai', $q);
	$this->db->from('pos_pembelian');
        return $this->db->count_all_results();
    }

    // get data with limit and search
    function get_limit_data($limit, $start = 0, $q = NULL) {
        // $this->db->order_by($this->id, $this->order);

	$this->db->or_like('tgl_pembelian', $q);
	$this->db->or_like('nama_suplier', $q);
	$this->db->or_like('nip_pegawai', $q);
	$this->db->limit($limit, $start);
        return $this->db->get('pos_pembelian')->result();
    }
    // get total rows
    function laporan_total_rows($q = NULL) {
        $this->db->from('pos_pembelian');
            return $this->db->count_all_results();
        }

    // get data with limit and search
    function laporan_get_limit_data($limit, $start = 0, $q = NULL) {
        $this->db->limit($limit, $start);
            return $this->db->get('pos_pembelian')->result();
        }
        function getAll_data() {
                return $this->db->get('pos_pembelian')->result();
            }
    // insert data
    function insert($data)
    {
        $this->db->insert($this->table, $data);
    }

    // update data
    function update($id, $data)
    {
        $this->db->where($this->id, $id);
        $this->db->update($this->table, $data);
    }

    // delete data
    function delete($id)
    {
        $this->db->where($this->id, $id);
        $this->db->delete($this->table);
    }

}

/* End of file Menu_model.php */
/* Location: ./application/models/Menu_model.php */