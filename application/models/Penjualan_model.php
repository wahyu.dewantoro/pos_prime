<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Penjualan_model extends CI_Model
{

    public $table = 'pos_penjualan';
    public $id = 'id';
    public $order = 'DESC';

    function __construct()
    {
        parent::__construct();
    }

    // get all
    function get_all()
    {
        $this->db->order_by($this->id, $this->order);
        return $this->db->get($this->table)->result();
    }

    // get data by id
    function get_by_id($id)
    {
        $this->db->where($this->id, $id);
        return $this->db->get($this->table)->row();
    }

    // get total rows
    function total_rows($q = NULL) {
        $this->db->like('id', $q);
	$this->db->or_like('nip', $q);
	$this->db->or_like('nama_pegawai', $q);
	$this->db->or_like('pelanggan_id', $q);
	$this->db->or_like('nama_pelanggan', $q);
	$this->db->or_like('tanggal_transaksi', $q);
	$this->db->from($this->table);
        return $this->db->count_all_results();
    }

    // get data with limit and search
    function get_limit_data($limit, $start = 0, $q = NULL) {
        $this->db->order_by($this->id, $this->order);
        $this->db->like('id', $q);
        $this->db->or_like('nip', $q);
        $this->db->or_like('nama_pegawai', $q);
        $this->db->or_like('pelanggan_id', $q);
        $this->db->or_like('nama_pelanggan', $q);
        $this->db->or_like('tanggal_transaksi', $q);
        $this->db->limit($limit, $start);
        $this->db->select($this->table.'.*,jumlahtransaksi(id) jumlah');
        return $this->db->get($this->table)->result();
    }

    // insert data
    function insert($data)
    {
        $this->db->insert($this->table, $data);
    }

    // update data
    function update($id, $data)
    {
        $this->db->where($this->id, $id);
        $this->db->update($this->table, $data);
    }

    // delete data
    function delete($id)
    {
        $this->db->where($this->id, $id);
        $this->db->delete($this->table);
    }
    function laporan_total_rows($q = NULL) {
	$this->db->from($this->table);
        return $this->db->count_all_results();
    }

    // get data with limit and search
    function laporan_get_limit_data($limit, $start = 0, $q = NULL) {
        $this->db->order_by($this->id, $this->order);
        $this->db->limit($limit, $start);
        $this->db->select($this->table.'.*,jumlahtransaksi(id) jumlah');
        return $this->db->get($this->table)->result();
    }
    function getAll_data() {
        $this->db->order_by($this->id, $this->order);
        $this->db->select($this->table.'.*,jumlahtransaksi(id) jumlah');
        return $this->db->get($this->table)->result();
    }
}

/* End of file Penjualan_model.php */
/* Location: ./application/models/Penjualan_model.php */