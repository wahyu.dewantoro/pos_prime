 <div class="row">
    <div class="col-sm-12">
        <div class="card-box">
        <h4 class="header-title mb-4">Transaksi</h4>
        <div class="row">
                            <a class="kotak" href="<?= base_url() ?>penjualan">
                            <div style="width:100px" class="kotak">
                                <div class=" tile-stats">
                                    <div class="icon"><i class="mdi mdi-cart-outline"><h6>Penjualan</h6> </i></div>

                                </div>
                            </div>
                            </a>
                            <a class="kotak" href="<?= base_url() ?>pembelian">
                            <div style="width:100px" class="kotak">
                                <div class=" tile-stats">
                                    <div class="icon"><i class="mdi mdi-cart"><h6>Pembelian</h6> </i></div>

                                </div>
                            </div>
                            </a>
                            <a class="kotak" href="<?= base_url() ?>penggajian">
                            <div style="width:100px" class="kotak">
                                <div class=" tile-stats">
                                    <div class="icon"><i class="mdi mdi-currency-usd"><h6>Penggajian</h6> </i></div>
                                </div>
                            </div>
                            </a>
                            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-12">
        <div class="card-box">
        <h4 class="header-title mb-4">Penjualan Terakhir</h4>
                    <div class="table-responsive">
                        <table class="table table-bordered" style="margin-bottom: 10px">
                            <thead class="thead-light">
                            <tr>
                                <th>No</th>
                                <th>Kode Transaksi</th>
                                <th>Kasir</th>
                                <th>Pelanggan</th>
                                <th>Tanggal Transaksi</th>
                                <th>Jumlah</th>
                                <th>Diskon</th>
                                <th>Bayar</th>
                                <th></th>
                                </tr>
                                </thead>
                            <tbody>
                            <?php
                            foreach ($penjualan_data as $penjualan)
                            {
                                ?>
                                <tr>
                        			<td align="center" width="80px"><?php echo ++$start ?></td>
                        			<td><?php echo $penjualan->kode_transaksi ?></td>
                        			<td><?php echo $penjualan->nama_pegawai ?></td>
                        			<td><?php echo $penjualan->nama_pelanggan ?></td>
                        			<td><?php echo $penjualan->tanggal_transaksi ?></td>
                                    <td align="right"><?php echo number_format($penjualan->jumlah,0,'','.') ?></td>
                                    <td align="right"><?php echo number_format($penjualan->diskon,0,'','.') ?></td>
                                    <td align="right"><?php echo number_format($penjualan->jumlah_bayar,0,'','.') ?></td>
                        			<td style="text-align:center" width="100px">
                        			     <!-- <button data-id="<?= $penjualan->id?>" onclick="struk(<?= $penjualan->id?>)" class="cetaks badge badge-warning"><i class="fa fa-file"></i></button> -->
                                         <?php
                                        //  echo anchor(site_url('penjualan/cetakstruk3/'.acak($penjualan->id)),'<i class="fa fa-print"></i>','class="badge badge-success" data-toggle="tooltip" data-placement="top" title="" data-original-title="Cetak Struk"');
                                         echo anchor(site_url('penjualan/detail/'.acak($penjualan->id)),'<i class="fa fa-eye"></i>','class="badge badge-info" data-toggle="tooltip" data-placement="top" title="" data-original-title="Detail"');
                                        //  if($akses['is_update']==1){
                        				// echo anchor(site_url('penjualan/update/'.acak($penjualan->id)),'<i class="fa fa-edit"></i>','class="badge badge-info" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit data"'); }
                        				//  if($akses['is_delete']==1){echo anchor(site_url('penjualan/delete/'.acak($penjualan->id)),'<i class="fa fa-trash"></i>','class="badge badge-danger" onclick="javasciprt: return confirm(\'Apakah anda yakin? data yang telah di hapus tidak dapat di kembalikan!\')" data-toggle="tooltip" data-placement="top" title="" data-original-title="Hapus data"'); }
                        				?>
                        			</td>
                        		</tr>
                                <?php
                            }
                            ?>
                            </tbody>
                        </table>
                        <div id="cetak"></div>
                    </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-12">
        <div class="card-box">
            <div id="a"></div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-12">
        <div class="card-box">
            <div id="b"></div>
        </div>
    </div>
</div>

<style>
.tile-stats {
    margin-bottom: 1px;
    margin-right:0px;
    padding-left:10px;
    padding-right:10px;
    padding-top:10px;
    padding-bottom:10px;
    border: solid #45b721;
    width:100px;
}
.tile-stats .icon {
    width: 20px;
    /* height: 100px; */
    color: #d26221;
    /* right: 17px; */
    left: 4px;
    top: 0px;
    font-size: 25px;
}
.widget-chart-two {
    min-height: 10px;
    width: 100px;
}
.kotak{
    padding-left:10px;
    padding-right:10px;

}
</style>