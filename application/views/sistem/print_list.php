<link href="<?= base_url() ?>highdmin/plugins/sweet-alert/sweetalert2.min.css" rel="stylesheet" type="text/css" />
 <div class="row">
    <div class="col-sm-12">
        <div class="card-box">
            <table class="table table-bordered table-hover">
                <thead>
                    <tr>
                        <th width="15px">#</th>
                        <th>IP Address</th>
                        <th width="80px">Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $no=1; foreach($data as $rk){ ?>
                        <tr>
                            <td><?= $no ?></td>
                            <td><?= $rk->ip_printer ?></td>
                            <td align="center">
                                <div class="btn-group">
                                    <?php if($akses['is_update']=='1'){
                                        echo anchor('printer/edit?q='.acak($rk->id_inc),'<i class="mdi mdi-table-edit"></i>','class="btn btn-sm btn-info" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit data"');
                                     }?>
                                </div>
                            </td>
                        </tr>
                    <?php $no++; } ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
  <!-- <button type="button" class="btn btn-light waves-effect waves-light btn-sm" id="sa-params">Click me</button> -->