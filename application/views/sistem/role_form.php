 <div class="row">
    <div class="col-sm-12">
        <div class="card-box">
            <form action="<?= $action ?>" method="POST" class="form-horizontal">
                 <div class="form-group row">
                    <label class="col-2 col-form-label">Nama Role <sup>*</sup></label>
                    <div class="col-3">
                        <input type="text" class="form-control" value="<?= $nama_role?>" name="nama_role" id="nama_role" required>
                    </div>
                </div>
                <input type="hidden" name="id_inc" value="<?= $id_inc ?>">
                 <div class="form-group mb-0 justify-content-end row">
                    <div class="col-10">
                        <button class="btn btn-sm btn-primary"><i class="fa fa-save"></i> Submit</button>
                        <?= anchor('role','<i class="fa fa-close"></i> Batal','class="btn btn-sm btn-warning"');?>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>