<link href="<?= base_url() ?>highdmin/plugins/sweet-alert/sweetalert2.min.css" rel="stylesheet" type="text/css" />
 <div class="row">
    <div class="col-sm-12">
        <div class="card-box">
            <table class="table table-bordered table-hover">
                <thead>
                    <tr>
                        <th width="15px">#</th>
                        <th>Nama Role</th>
                        <th width="80px">Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $no=1; foreach($data as $rk){ ?>
                        <tr>
                            <td><?= $no ?></td>
                            <td><?= $rk->nama_role ?></td>
                            <td align="center">
                                <div class="btn-group">
                                    <?php if($akses['is_update']=='1'){
                                         echo anchor('role/setting?q='.acak($rk->id_inc),'<i class="mdi mdi-folder-lock"></i>','class="btn btn-sm btn-warning" data-toggle="tooltip" data-placement="top" title="" data-original-title="Atur hak akses"');
                                        echo anchor('role/edit?q='.acak($rk->id_inc),'<i class="mdi mdi-table-edit"></i>','class="btn btn-sm btn-info" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit data"');
                                     }?>
                                    <?php if($akses['is_delete']=='1'){ 
                                        // mdi mdi-delete
                                        echo anchor('role/delete?q='.acak($rk->id_inc),'<i class="mdi mdi-delete"></i>','class="btn btn-sm btn-danger" onclick="return confirm(\'Apakah anda yakin?\')" data-toggle="tooltip" data-placement="top" title="" data-original-title="Hapus data"');
                                    }?>
                                </div>
                            </td>
                        </tr>
                    <?php $no++; } ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
  <!-- <button type="button" class="btn btn-light waves-effect waves-light btn-sm" id="sa-params">Click me</button> -->