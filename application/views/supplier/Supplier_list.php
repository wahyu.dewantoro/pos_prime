<div class="row">
            <div class="col-12">
                <div class="card-box">
                    <div class="row">
                        <div class="col-md-12 ">
                             <form action="<?php echo site_url('supplier'); ?>" class="form-horizontal" method="get">
                                <div class="form-group row">
                                    <label class="col-1 col-form-label">Pencarian</label>
                                    <div class="col-4">
                                        <input type="text" class="form-control form-control-sm" name="q" value="<?php echo $q; ?>">
                                    </div>
                                    <div class="col-4">
                                        <?php  if ($q <> '') {   ?>
                                                <a href="<?php echo site_url('supplier'); ?>" class="btn btn-warning"><i class="fa fa-refresh"></i> Reset</a>
                                        <?php   } ?>
                                        <button class="btn btn-primary" type="submit"><i class="fa fa-search"></i> Cari</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-bordered" style="margin-bottom: 10px">
                            <thead class="thead-light">
                            <tr>
                                <th>No</th>
                                <th>Nama</th>
                                <th>Telp</th>
                                <th>Alamat</th>
                                <th></th>
                                </tr>
                                </thead>
                            <tbody>
                            <?php
                            foreach ($supplier_data as $menu)
                            {
                                ?>
                                <tr>
                        			<td align="center" width="80px"><?php echo ++$start ?></td>
                        			<td><?php echo $menu->nama ?></td>
                        			<td><?php echo $menu->hp ?></td>
                        			<td><?php echo $menu->alamat ?></td>
                        			<td style="text-align:center" width="100px">
                        				<?php if($akses['is_update']==1){
                        				echo anchor(site_url('supplier/update/'.acak($menu->id)),'<i class="fa fa-edit"></i>','class="badge badge-info" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit data"'); }
                        				 if($akses['is_delete']==1){echo anchor(site_url('supplier/delete/'.acak($menu->id)),'<i class="fa fa-trash"></i>','class="badge badge-danger" onclick="javasciprt: return confirm(\'Apakah anda yakin? data yang telah di hapus tidak dapat di kembalikan!\')" data-toggle="tooltip" data-placement="top" title="" data-original-title="Hapus data"'); }
                        				?>
                        			</td>
                        		</tr>
                                <?php
                            }
                            ?>
                            </tbody>
                        </table>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <a href="#" class="btn btn-success">Total Data : <?php echo $total_rows ?></a>
	</div>
                        <div class="col-md-6 text-right">
                            <?php echo $pagination ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>