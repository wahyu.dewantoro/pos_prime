<div class="row">
            <div class="col-12">
                <div class="card-box">
                    <form action="<?php echo $action; ?>" method="post"  class="form-horizontal" role="form">

                        <div class="form-group row">
                            <label for="nama" class="col-3 col-form-label">Nama Menu <sup style="color:red;">*</sup></label>
                            <div class="col-9">
                                <input type="text" class="form-control"  name="nama" id="nama" placeholder="Nama" value="<?php echo $nama; ?>">
                                <?php echo form_error('nama') ?>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="hp" class="col-3 col-form-label">Telp <sup style="color:red;">*</sup></label>
                            <div class="col-9">
                                <input type="number" class="form-control"  name="hp" id="hp" placeholder="Telp" value="<?php echo $hp; ?>">
                                <?php echo form_error('hp') ?>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="alamat" class="col-3 col-form-label">Alamat <sup style="color:red;">*</sup></label>
                            <div class="col-9">
                                <textarea class="form-control"  name="alamat" id="alamat" placeholder="Alamat" cols="30" rows="4"><?php echo $alamat; ?></textarea>
                                <?php echo form_error('alamat') ?>
                            </div>
                        </div>
                        <input type="hidden" name="id" value="<?= $id ?>">
	                   <div class="form-group mb-0 justify-content-end row">
                            <div class="col-9">
                                <button type="submit" class="btn btn-success waves-effect waves-light"><i class="fa fa-save"></i> Submit</button>
                                <button type="reset" class="btn btn-warning waves-effect waves-light"><i class="fa fa-refresh"></i> Reset</button>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>