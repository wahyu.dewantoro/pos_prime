<?php $namabulan=array(
      '',
      'Januari',
      'Februari',
      'Maret',
      'April',
      'Mei',
      'Juni',
      'Juli',
      'Agustus',
      'September',
      'Oktober',
      'November',
      'Desember'
      ) ?>
<div class="row">
            <div class="col-12">
                <div class="card-box">
                    <form action="<?php echo $action; ?>" method="post"  class="form-horizontal" role="form">

                        <div class="form-group row">
                            <label for="tanggal_transaksi" class="col-2 col-form-label">Tanggal Transaksi <sup style="color:red;">*</sup></label>
                            <div class="col-2">
                                <input type="date" class="form-control"  name="tanggal_transaksi" id="tanggal_transaksi" placeholder="Tanggal Pembelian" value="<?php echo $tanggal_transaksi; ?>">
                                <?php echo form_error('tanggal_transaksi') ?>
                            </div>
                            <label for="bulan_gaji" class="col-2 col-form-label">Bulan Gaji <sup style="color:red;">*</sup></label>
                            <div class="col-2">
                                <select id="bulan_gaji"  name="bulan_gaji" required="required" placeholder="Bulan Gaji" class="form-control select2 col-md-7 col-xs-12">
                                    <option value="">Pilih</option>
                                    <?php for ($i=1; $i < 13 ; $i++) { ?>
                                    <option <?php if(!empty($bulan_gaji)){ if($i==$bulan_gaji){echo "selected";}} else {if($i==date("m")){echo "selected";}}?>  value="<?php echo $i; ?>"><?php echo $namabulan[$i] ?></option>
                                    <?php } ?>
                                </select>
                                <?php echo form_error('bulan_gaji') ?>
                            </div>
                            <label for="tahun_gaji" class="col-2 col-form-label">Tahun Gaji <sup style="color:red;">*</sup></label>
                            <div class="col-2">
                                <select id="tahun_gaji"  name="tahun_gaji" required="required" placeholder="Bulan Gaji" class="form-control select2 col-md-7 col-xs-12">
                                    <option value="">Pilih</option>
                                    <?php for ($i=2019; $i < 2025 ; $i++) { ?>
                                    <option <?php if(!empty($tahun_gaji)){ if($i==$tahun_gaji){echo "selected";}} else {if($i==date("Y")){echo "selected";}}?>  value="<?php echo $i; ?>"><?php echo $i?></option>
                                    <?php } ?>
                                </select>
                                <?php echo form_error('tahun_gaji') ?>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="nip_pegawai" class="col-2 col-form-label">NIP Pegawai <sup style="color:red;">*</sup></label>
                            <div class="col-2">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text carinip" id="basic-addon1"><i class="fa fa-search"></i></span>
                                    </div>
                                    <input readonly type="text" class="form-control carinip"  name="nip_pegawai" id="nip_pegawai" placeholder="NIP Pegawai" value="<?php echo $nip_pegawai; ?>">

                                </div>
                                <?php echo form_error('nip_pegawai') ?>
                            </div>
                            <label for="nama_pegawai" class="col-2 col-form-label">Nama Pegawai <sup style="color:red;">*</sup></label>
                            <div class="col-2">
                                <input readonly type="text" class="form-control"  name="nama_pegawai" id="nama_pegawai" placeholder="Nama Pegawai" value="<?php echo $nama_pegawai; ?>">
                                <?php echo form_error('nama_pegawai') ?>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="gaji_pokok" class="col-2 col-form-label">Gaji Pokok <sup style="color:red;">*</sup></label>
                            <div class="col-2">
                                <input readonly type="text" class="form-control"  name="gaji_pokok" id="gaji_pokok" placeholder="Gaji Pokok" value="<?php echo $gaji_pokok; ?>">
                                <?php echo form_error('gaji_pokok') ?>
                            </div>
                            <label for="tunjangan" class="col-2 col-form-label">Tunjangan <sup style="color:red;">*</sup></label>
                            <div class="col-2">
                                <input readonly type="text" class="form-control"  name="tunjangan" id="tunjangan" placeholder="Tunjangan" value="<?php echo $tunjangan; ?>">
                                <?php echo form_error('tunjangan') ?>
                            </div>
                            <label for="bonus" class="col-2 col-form-label">Bonus <sup style="color:red;">*</sup></label>
                            <div class="col-2">
                                <input readonly type="text" class="form-control"  name="bonus" id="bonus" placeholder="Bonus" value="<?php echo $bonus; ?>">
                                <?php echo form_error('bonus') ?>
                            </div>
                        </div>
	    <input type="hidden" name="id" value="<?php echo $id; ?>" />
	<div class="form-group mb-0 justify-content-end row">
                            <div class="col-12">
                                <button type="submit" class="btn btn-success waves-effect waves-light"><i class="fa fa-save"></i> Submit</button>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
        <!-- Modal -->
        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog" style="width:800px">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">List Pegawai</h4>
                    </div>
                    <div class="modal-body">
                        <table id="lookup" class="table table-bordered table-hover table-striped">
                            <thead>
                                <tr>
                                    <th>NIP</th>
                                <th>Nama</th>
                                <th>Hp</th>
                                <th>Mulai Kerja</th>
                                </tr>
                            </thead>
                            <tbody>
                                  <?php
                            foreach ($pegawai as $pegawai)
                            {
                                ?>
                                <tr class="pilih" data-nip="<?php echo $pegawai->nip ?>" data-nama_pegawai="<?php echo $pegawai->nama ?>"
                                data-bonus="<?php echo $pegawai->bonus ?>" data-gaji_pokok="<?php echo $pegawai->gaji_pokok ?>" data-tunjangan="<?php echo $pegawai->tunjangan ?>">
                                    <td><?php echo $pegawai->nip ?></td>
                                    <td><?php echo $pegawai->nama ?></td>
                                    <td><?php echo $pegawai->hp ?></td>
                                    <td><?php echo date_indo($pegawai->tgl_mulai_kerja) ?></td>
                                </tr>
                                <?php
                            }
                            ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>