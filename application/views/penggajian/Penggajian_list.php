<?php $namabulan=array(
      '',
      'Januari',
      'Februari',
      'Maret',
      'April',
      'Mei',
      'Juni',
      'Juli',
      'Agustus',
      'September',
      'Oktober',
      'November',
      'Desember'
      ) ?>
<div class="row">
            <div class="col-12">
                <div class="card-box">
                    <div class="row">
                        <div class="col-md-12 ">
                             <form action="<?php echo site_url('penggajian'); ?>" class="form-horizontal" method="get">
                                <div class="form-group row">
                                    <label class="col-1 col-form-label">Pencarian</label>
                                    <div class="col-4">
                                        <input type="text" class="form-control form-control-sm" name="q" value="<?php echo $q; ?>">
                                    </div>
                                    <div class="col-4">
                                        <?php  if ($q <> '') {   ?>
                                                <a href="<?php echo site_url('penggajian'); ?>" class="btn btn-warning"><i class="fa fa-refresh"></i> Reset</a>
                                        <?php   } ?>
                                        <button class="btn btn-primary" type="submit"><i class="fa fa-search"></i> Cari</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-bordered" style="margin-bottom: 10px">
                            <thead class="thead-light">
                            <tr>
                                <th>No</th>
		<th>Tanggal Transaksi</th>
		<th>Bulan Gaji</th>
		<th>Tahun Gaji</th>
		<th>Nip Pegawai</th>
		<th>Nama Pegawai</th>
		<th>Gaji Pokok</th>
		<th>Tunjangan</th>
		<th>Bonus</th>
		<th></th>
                                </tr>
                                </thead>
                            <tbody>
                            <?php
                            foreach ($penggajian_data as $penggajian)
                            {
                                ?>
                                <tr>
			<td align="center" width="80px"><?php echo ++$start ?></td>
			<td><?php echo date_indo($penggajian->tanggal_transaksi) ?></td>
			<td><?php echo $namabulan[$penggajian->bulan_gaji] ?></td>
			<td class="text-right"><?php echo $penggajian->tahun_gaji ?></td>
			<td class="text-right"><?php echo $penggajian->nip_pegawai ?></td>
			<td><?php echo $penggajian->nama_pegawai ?></td>
			<td class="text-right"><?php echo number_format($penggajian->gaji_pokok,0,",",".") ?></td>
			<td class="text-right"><?php echo number_format($penggajian->tunjangan,0,",",".") ?></td>
			<td class="text-right"><?php echo number_format($penggajian->bonus,0,",",".") ?></td>
			<td style="text-align:center" width="100px">
				<?php if($akses['is_update']==1){
				echo anchor(site_url('penggajian/update/'.acak($penggajian->id)),'<i class="fa fa-edit"></i>','class="badge badge-info" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit data"'); }
				 if($akses['is_delete']==1){echo anchor(site_url('penggajian/delete/'.acak($penggajian->id)),'<i class="fa fa-trash"></i>','class="badge badge-danger" onclick="javasciprt: return confirm(\'Apakah anda yakin? data yang telah di hapus tidak dapat di kembalikan!\')" data-toggle="tooltip" data-placement="top" title="" data-original-title="Hapus data"'); }
				?>
			</td>
		</tr>
                                <?php
                            }
                            ?>
                            </tbody>
                        </table>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <a href="#" class="btn btn-success">Total Data : <?php echo $total_rows ?></a>
	</div>
                        <div class="col-md-6 text-right">
                            <?php echo $pagination ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>