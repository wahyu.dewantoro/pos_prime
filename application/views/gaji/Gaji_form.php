<div class="row">
            <div class="col-12">
                <div class="card-box">
                    <form action="<?php echo $action; ?>" method="post"  class="form-horizontal" role="form">
	    
                        <div class="form-group row">
                            <label for="nip" class="col-3 col-form-label">NIP <sup style="color:red;">*</sup></label>
                            <div class="col-9">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text carinip" id="basic-addon1"><i class="fa fa-search"></i></span>
                                    </div>
                                    <input readonly type="text" class="form-control carinip"  name="nip" id="nip" placeholder="NIP Pegawai" value="<?php echo $nip; ?>">
                                    
                                </div>

                                
                                <?php echo form_error('nip') ?>                                
                            </div>
                        </div>
	    
                        <div class="form-group row">
                            <label for="nama_pegawai" class="col-3 col-form-label">Nama Pegawai <sup style="color:red;">*</sup></label>
                            <div class="col-9">
                                <input type="text" readonly class="form-control"  name="nama_pegawai" id="nama_pegawai" placeholder="Nama Pegawai" value="<?php echo $nama_pegawai; ?>">
                                <?php echo form_error('nama_pegawai') ?>                                
                            </div>
                        </div>
	    
                        <div class="form-group row">
                            <label for="gaji_pokok" class="col-3 col-form-label">Gaji Pokok <sup style="color:red;">*</sup></label>
                            <div class="col-9">
                                <input type="text" class="form-control" onkeyup="formatangka(this)"  name="gaji_pokok" id="gaji_pokok" placeholder="Gaji Pokok" value="<?php echo $gaji_pokok; ?>">
                                <?php echo form_error('gaji_pokok') ?>                                
                            </div>
                        </div>
	    
                        <div class="form-group row">
                            <label for="tunjangan" class="col-3 col-form-label">Tunjangan <sup style="color:red;">*</sup></label>
                            <div class="col-9">
                                <input type="text" class="form-control"  onkeyup="formatangka(this)" name="tunjangan" id="tunjangan" placeholder="Tunjangan" value="<?php echo $tunjangan; ?>">
                                <?php echo form_error('tunjangan') ?>                                
                            </div>
                        </div>
	    
                        <div class="form-group row">
                            <label for="bonus" class="col-3 col-form-label">Bonus <sup style="color:red;">*</sup></label>
                            <div class="col-9">
                                <input type="text" class="form-control"  onkeyup="formatangka(this)" name="bonus" id="bonus" placeholder="Bonus" value="<?php echo $bonus; ?>">
                                <?php echo form_error('bonus') ?>                                
                            </div>
                        </div>
                        <input type="hidden" name="id" value="<?php echo $id; ?>" /> 
                        <div class="form-group mb-0 justify-content-end row">
                            <div class="col-9">
                                <button type="submit" class="btn btn-success waves-effect waves-light"><i class="fa fa-save"></i> Submit</button>
                                <button type="reset" class="btn btn-warning waves-effect waves-light"><i class="fa fa-refresh"></i> Reset</button>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>

        <!-- Modal -->
        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog" style="width:800px">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">List Pegawai</h4>
                    </div>
                    <div class="modal-body">
                        <table id="lookup" class="table table-bordered table-hover table-striped">
                            <thead>
                                <tr>
                                    <th>NIP</th>
                                <th>Nama</th>
                                <th>Hp</th>
                                <th>Mulai Kerja</th>
                                </tr>
                            </thead>
                            <tbody>
                                  <?php
                            foreach ($pegawai as $pegawai)
                            {
                                ?>
                                <tr class="pilih" data-nip="<?php echo $pegawai->nip ?>" data-nama_pegawai="<?php echo $pegawai->nama ?>">
                                    <td><?php echo $pegawai->nip ?></td>
                                    <td><?php echo $pegawai->nama ?></td>
                                    <td><?php echo $pegawai->hp ?></td>
                                    <td><?php echo date_indo($pegawai->tgl_mulai_kerja) ?></td>
                                </tr>
                                <?php
                            }
                            ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>