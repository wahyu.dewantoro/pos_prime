<script type="text/javascript">
  $(function () {
    Highcharts.setOptions({
		lang: {
			thousandsSep: '.'
		}
	});
      $('#a').highcharts({
          chart: {
              type: 'column'
          },
          title: {
              text: 'Penjualan Harian <?php echo bulan(date("m"))." ".date("Y") ?>'
          },
          xAxis: {
              categories: [
                                <?php foreach ($harian as $rk ) {
                                    echo "'$rk->tgl',";
                                }  ?>
                            ],
              crosshair: true
          },
          // yAxis: {
          //     min: 0,
          //     title: {
          //         text: 'Nilai Usulan (Rp)'
          //     }
          // },
          yAxis: {

            labels: {
                formatter: function() {
                    var ret,
                        numericSymbols = [' Ribu', ' Juta', ' Milyar', ' Triliun', 'P', 'E'],
                        i = 6;
                    if(this.value >=1000) {
                        while (i-- && ret === undefined) {
                            multi = Math.pow(1000, i + 1);
                            if (this.value >= multi && numericSymbols[i] !== null) {
                                ret = (this.value / multi) + numericSymbols[i];
                            }
                        }
                    }
                    return 'Rp ' + (ret ? ret : this.value);
                }
            },
                          title: {
                  text: 'Jumlah (Rp)'
              }

        },
          tooltip: {
              pointFormat: ' <b>{point.y:,.0f} </b>',
              shared: true,
          },
          plotOptions: {
              column: {
                  pointPadding: 0.2,
                  borderWidth: 0
              }
          },
          credits: {
                enabled: false
            },
          series: [{
              name: '',
                showInLegend: false,
              data: [

                <?php foreach ($harian as $rk ) {
                                    echo "$rk->jumlah,";
                                }  ?>
                          ]

          }]
      });
  });
      </script>
<script type="text/javascript">
  $(function () {
    Highcharts.setOptions({
		lang: {
			thousandsSep: '.'
		}
	});
      $('#b').highcharts({
          chart: {
              type: 'column'
          },
          title: {
              text: 'Penjualan Bulanan Tahun <?php echo date("Y") ?>'
          },
          xAxis: {
              categories: [
                                <?php foreach ($bulanan as $rk ) {
                                    echo "'".bulan($rk->BULAN)."',";
                                }  ?>
                            ],
              crosshair: true
          },
          // yAxis: {
          //     min: 0,
          //     title: {
          //         text: 'Nilai Usulan (Rp)'
          //     }
          // },
          yAxis: {

            labels: {
                formatter: function() {
                    var ret,
                        numericSymbols = [' Ribu', ' Juta', ' Milyar', ' Triliun', 'P', 'E'],
                        i = 6;
                    if(this.value >=1000) {
                        while (i-- && ret === undefined) {
                            multi = Math.pow(1000, i + 1);
                            if (this.value >= multi && numericSymbols[i] !== null) {
                                ret = (this.value / multi) + numericSymbols[i];
                            }
                        }
                    }
                    return 'Rp ' + (ret ? ret : this.value);
                }
            },
                          title: {
                  text: 'Jumlah (Rp)'
              }

        },
          tooltip: {
              pointFormat: ' <b>{point.y:,.0f} </b>',
              shared: true,
          },
          plotOptions: {
              column: {
                  pointPadding: 0.2,
                  borderWidth: 0
              }
          },
          credits: {
                enabled: false
            },
          series: [{
              name: '',
                showInLegend: false,
              data: [

                <?php foreach ($bulanan as $rk ) {
                                    echo "$rk->jumlah,";
                                }  ?>
                          ], color: '#FF0000'


          }]
      });
  });
      </script>
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
