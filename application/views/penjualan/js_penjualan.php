<span id="cetak"></span>
<script type="text/javascript">
    qz.security.setCertificatePromise(function(resolve, reject) {
       $.ajax({ url: "<?= base_url() ?>assets/override.crt"}).then(resolve, reject);
    });
    qz.security.setSignaturePromise(function(toSign) {
        return function(resolve, reject) {
        $.post("<?= base_url() ?>assets/sign-message.php", {request: toSign}).then(resolve, reject);
        };
    });
    qz.websocket.connect().then(function() {
        // alert("Connected!");
        qz.printers.find("POS-80").then(function(found) {
            // alert("Printer: " + found);
            $('#jenisprint').val(found);
        }).catch(function(e) { alert('Tidak Ditemukan') });
    });
    $('body').on('keypress', 'input, select, textarea', function(e) {
        var self = $(this)
          , form = self.parents('form:eq(0)')
          , focusable
          , next
          ;
        if (e.keyCode == 13) {
            focusable = form.find('input,select,button,textarea').filter(':visible');
            next = focusable.eq(focusable.index(this)+1);
            if (next.length) {
                next.focus();
            } else {
                form.submit();
            }
            return false;

        }
    });




    $(document).on('click', '#add', function (e) {
        e.preventDefault();
        vproduk =$('#produk').val();
        vqty    =$('#qty').val();
        vtotal  =$('#vtotal').val();

        $.ajax({
            type:'post',
            url: "<?php echo base_url()?>penjualan/cekproduk",
            data:{'produk':vproduk,'qty':vqty},
            success: function(msg){
                if(msg==0){
                    addtotal(vproduk,vqty,vtotal);
                    $.ajax({
                        type:'post',
                        url: "<?php echo base_url()?>penjualan/appendproduk",
                        data:{'produk':vproduk,'qty':vqty},
                        success: function(msg){
                             $(".project_images").append(msg);

                        }
                    });

                $("#produk").focus();
                $("#qty").val('');
                $("#produk").val('');
                } else {
                    if (confirm('anda menambahkan item yang sama dg yg sudah ada?')) {
                        addtotal(vproduk,vqty,vtotal);
                        $.ajax({
                            type:'post',
                            url: "<?php echo base_url()?>penjualan/appendproduk",
                            data:{'produk':vproduk,'qty':vqty},
                            success: function(msg){
                                $(".project_images").append(msg);

                            }
                        });
                    $("#produk").focus();
                $("#produk").val('');
                    $("#qty").val('');
                    } else {
                        // Do nothing!
                    }
                }

            }
        });
        // addtotal(vproduk,vqty,vtotal);
        // $.ajax({
        //     type:'post',
        //     url: "<?php echo base_url()?>penjualan/appendproduk",
        //     data:{'produk':vproduk,'qty':vqty},
        //     success: function(msg){
        //          $(".project_images").append(msg);

        //     }
        // });

    });


    $("#diskon").keyup(function(){
      // $("input").css("background-color", "pink");
      diskon=$('#diskon').val();
      total=$('#vtotal').val();
      getjumlah(total,diskon);
    });

    function getjumlah(total,diskon){
           a = total.replace(/[^\d]/g,"");
           b =diskon.replace(/[^\d]/g,"")
           c=a-b;
           $('#jumlah_bayar').val(ribuan(c.toString()));
    }

    function addtotal(produk,qty,total){
        // alert(produk)
        // alert(qty)
        // alert(total)
        if(total==''||total==null){
            total=0
        }
        $.ajax({
            type:'post',
            url: "<?php echo base_url()?>penjualan/tambahtotal",
            data:{'produk':vproduk,'qty':vqty,'total':total},
            success: function(hasil){
                diskon=$('#diskon').val();
                getjumlah(hasil,diskon);
                $('#vtotal').val(hasil);
            }
        });
    }


    $('#penjualan').submit(function(e) {
        if (confirm('data yg anda masukan sudah benar???')) {

            $("#cetak").html('');
        // e.preventDefault();
        $.ajax({
            type: 'POST',
            url: "<?= $action ?>",
            data: $(this).serialize(),
            success: function(data) {
                // $('#result').html(data);
                // alert(data)
                if(data==0){
                    $('#result').html('');
                    $.toast({
                         heading  : 'Warning!',
                         text     : 'data gagal di proses',
                         position : 'top-right',
                         loaderBg : '#5ba035',
                         icon     : 'warning',
                         hideAfter: 3000,
                    });


                }else{
                    $('#penjualan').closest('form').find("input[type=text], textarea").val("");
                    $('.project_images').html('');
                    $("#cetak").html(data).hide();
                    qz.printers.find("POS-80").then(function(found) {
                        // alert("Printer: " + found);
                        $('#jenisprint').val(found);
                    }).catch(function(e) { alert('Tidak Ditemukan') });
                    // printDiv('');
                    $.ajax({
            type:'get',
            url: "<?php echo base_url()?>penjualan/cetakstruk3/"+data,
            success: function(data){
                var config = qz.configs.create("POS-80");
                var data = [
                    {
                        type: 'raw',
                        format: 'file',
                        data: '<?= base_url().get_userdata("app_id_pengguna") ?>print.tmp'
                    }
                ];


                qz.print(config, data).then(function() {
                    // alert("Sent data to printer");
                    $.ajax({
                    type:'get',
                    url: "<?php echo base_url()?>penjualan/hapusfile/",
                    success: function(data){
                        alert('Cetak Berhasil')

                    }
                });
                }).catch(function(e) { alert('Cetak Gagal') });

            }
        });
                }
            }
        })
        return false;
        } else {

        }
    });



    window.onafterprint = function(){
      window.location.reload(true);
    $("#produk").focus();
    }

            // Remove parent of 'remove' link when link is clicked.
        $('.project_images').on('click', '.remove_project_file', function(e) {
            if (confirm('Yakin Menghapus?')) {

                e.preventDefault();
            kurang=$(this).attr('data-jumlah');
            insert_id=$(this).attr('data-insert_id');
            vtotal=$('#vtotal').val();
            total=vtotal.replace('.','');
             $.ajax({
                type:'post',
                url: "<?php echo base_url()?>penjualan/kurangtotal",
                data:{'total':total,'kurang':kurang,'insert_id':insert_id},
                success: function(hasil){
                diskon=$('#diskon').val();
                getjumlah(hasil,diskon);
                 $('#vtotal').val(hasil);
                }
            });

            $(this).closest('tr').remove();
            } else {

            }
        });



     function printDiv(divName){
        var printContents = document.getElementById(divName).innerHTML;
        var originalContents = document.body.innerHTML;
        document.body.innerHTML = printContents;
        window.print();
        document.body.innerHTML = originalContents;
    }

    function ribuan(obj){
     // a       = obj;
     b       = obj.toString();
     c       = "";
     panjang = b.length;
     j       = 0;
     for (i = panjang; i > 0; i--) {
       j = j + 1;
       if (((j % 3) == 1) && (j != 1)) {
         c = b.substr(i-1,1) + "." + c;
       } else {
         c = b.substr(i-1,1) + c;
       }
     }

     return c;
  };

  $("#produk").focus();
  jQuery.extend(jQuery.expr[':'], {
    focusable: function (el, index, selector) {
        return $(el).is('a, button, :input, [tabindex]');
    }
});

$(document).on('keypress', 'input,select', function (e) {
    if (e.which == 13) {
        e.preventDefault();
        // Get all focusable elements on the page
        var $canfocus = $(':focusable');
        var index = $canfocus.index(document.activeElement) + 1;
        if (index >= $canfocus.length) index = 0;
        $canfocus.eq(index).focus();
    }
});
</script>