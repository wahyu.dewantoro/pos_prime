
<script type="text/javascript">
    qz.security.setCertificatePromise(function(resolve, reject) {
       $.ajax({ url: "<?= base_url() ?>assets/override.crt"}).then(resolve, reject);
    });
    qz.security.setSignaturePromise(function(toSign) {
        return function(resolve, reject) {
        $.post("<?= base_url() ?>assets/sign-message.php", {request: toSign}).then(resolve, reject);
        };
    });
    qz.websocket.connect().then(function() {
        // alert("Connected!");
        qz.printers.find("POS-80").then(function(found) {
            // alert("Printer: " + found);
            $('#jenisprint').val(found);
        }).catch(function(e) { alert('Tidak Ditemukan') });
    });
    function printStuff(id) {

        $.ajax({
            type:'get',
            url: "<?php echo base_url()?>penjualan/cetakstruk3/"+id,
            success: function(data){
                var config = qz.configs.create("POS-80");
                var data = [
                    {
                        type: 'raw',
                        format: 'file',
                        data: '<?= base_url().get_userdata("app_id_pengguna") ?>print.tmp'
                    }
                ];


                qz.print(config, data).then(function() {
                    // alert("Sent data to printer");
                    $.ajax({
                    type:'get',
                    url: "<?php echo base_url()?>penjualan/hapusfile/",
                    success: function(data){
                        alert('Cetak Berhasil')

                    }
                });
                }).catch(function(e) { alert('Cetak Gagal') });

            }
        });


    }

    function displayError(err) {
        console.error(err);
        displayMessage(err, 'alert-danger');
    }

    function displayMessage(msg, css) {
        if (css == undefined) { css = 'alert-info'; }

        var timeout = setTimeout(function() { $('#' + timeout).alert('close'); }, 5000);

        var alert = $("<div/>").addClass('alert alert-dismissible fade in ' + css)
                .css('max-height', '20em').css('overflow', 'auto')
                .attr('id', timeout).attr('role', 'alert');
        alert.html("<button type='button' class='close' data-dismiss='alert'>&times;</button>" + msg);

        $("#qz-alert").append(alert);
    }

    $(document).on('click', '.cetaks', function (e) {
        e.preventDefault();
        $("#cetak").html('');
        $.ajax({
            type:'get',
            url: "<?php echo base_url()?>penjualan/cetakstruk",
            data:{'data':$(this).attr('data-id')},
            success: function(data){
                 // $(".project_images").append(msg);
							$("#cetak").html(data).hide();
							printDiv('cetak');

            }
        });


    });

     function printDiv(divName){
        var printContents = document.getElementById(divName).innerHTML;
        var originalContents = document.body.innerHTML;
        document.body.innerHTML = printContents;
        window.print();
        document.body.innerHTML = originalContents;
    }

</script>