<style type="text/css">
    .inputkanan {
    text-align: right;
}
</style>
<div class="row">
    <div class="col-12">
        <div class="card-box">
            <form  method="post"  role="form" id="penjualan">
                <div class="row">
                   <div class="col-3">
                       <div class="form-group row">
                            <label class="col-4 col-form-label">Print</label>
                           <div class="col-8">
                               <input type="text" readonly id="jenisprint" class="form-control" >
                           </div>
                       </div>
                       <div class="form-group row">
                            <label class="col-4 col-form-label">Produk</label>
                            <div class="col-8">
                                <select id="produk" class="form-control" >
                                    <option value="">Produk</option>
                                    <?php foreach($produk as $pr){?>
                                        <option value="<?= $pr->id ?>"><?= $pr->nama_produk.'/ '.$pr->satuan." (".angka($pr->harga).")" ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                       </div>
                       <div class="form-group row">
                            <label class="col-4 col-form-label">QTY</label>
                           <div class="col-8">
                               <input type="number" id="qty" class="form-control" >
                           </div>
                       </div>
                       <div class="form-group row">
                            <label class="col-4 col-form-label"></label>
                           <div class="col-8">
                               <button id="add" class="btn btn-sm btn-success"><i class="fa fa-plus"></i> Tambahkan</button>
                           </div>
                       </div>
                   </div>
                   <div class="col-6">
                       <table class="table">
                           <thead>
                               <tr>
                                   <td align="center"><b>Qty</b></td>
                                   <td align="left"><b>Item</b></td>
                                   <td align="right"><b>Jumlah</b></td>
                               </tr>
                           </thead>
                            <tbody class="project_images">
                            </tbody>
                            <tfoot>
                                <tr>
                                    <td colspan="2"><b>Sub total</b></td>
                                    <td align="right"><input  type="text" class="form-control inputkanan" disabled="" value="0" id="vtotal" tabindex="-1" ></td>
                                </tr>
                                <tr>
                                    <td colspan="2"><b>Diskon</b></td>
                                    <td align="right"><input  type="text" class="form-control inputkanan" onkeyup="formatangka(this)" value="0" id="diskon" name="diskon"></td>
                                </tr>
                                <tr>
                                    <td colspan="2"><b>Jumlah Bayar</b></td>
                                    <td align="right"><input  type="text" class="form-control inputkanan" readonly  value="0" id="jumlah_bayar" tabindex="-1"  name="jumlah_bayar"></td>
                                </tr>
                            </tfoot>
                        </table>
                   </div>
                   <div class="col-3">
                        <input type ="hidden" readonly class="form-control"  name="nip" id="nip" placeholder="NIP" value="<?php echo $nip; ?>">
                        <input type ="hidden" readonly class="form-control"  name="nama_pegawai" id="nama_pegawai" placeholder="Nama Pegawai" value="<?php echo $nama_pegawai; ?>">
                        <div class="form-group row">
                            <label class="col-4 col-form-label"> Custumer<sup style="color: red">*</sup></label>
                            <div class="col-8">
                                <input type="text" name="nama_pelanggan" id="nama_pelanggan" class="form-control" required="">
                                <?= form_error('nama_pelanggan'); ?>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-4 col-form-label">No HP <sup style="color: red">*</sup></label>
                            <div class="col-8">
                                <input type="text" name="no_hp" id="no_hp" class="form-control" required="">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-4 col-form-label">Instansi </label>
                            <div class="col-8">
                                <input type="text" name="instansi" id="instansi" class="form-control">

                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-4 col-form-label">Email </label>
                            <div class="col-8">
                                <input type="email" name="email" id="email" class="form-control">
                            </div>
                        </div>

                        <div class="pull-right">
                                <button type="submit" id="submit" class="btn btn-success waves-effect waves-light"><i class="fa fa-save"></i> Proses</button>
                                <button type="reset" class="btn btn-warning waves-effect waves-light"><i class="fa fa-refresh"></i> Reset</button>
                        </div>
                   </div>
                </div>





                       <input type="hidden" name="id" value="<?php echo $id; ?>" />


            </form>
        </div>
    </div>
</div>
