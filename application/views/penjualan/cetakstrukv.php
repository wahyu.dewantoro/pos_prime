<style type="text/css">
	@media print {
  @page {
    size: 70mm 210mm; /* landscape */
    /* you can also specify margins here: */
    margin: 25mm;
    margin-right: 45mm; /* for compatibility with both A4 and Letter */
  }
}

</style>


<h3  style="text-align: center;">PRIME Digital Printing</h3>
<p  style="text-align: center;">The best solutions for printing</p>
<table style="font-size: 12">
	<tr>
		<td>Tanggal</td>
		<td>: <?= $head->tanggal_transaksi?></td>
	</tr>
	<tr>
		<td>Kode</td>
		<td>: <?= $head->kode_transaksi ?></td>
	</tr>
	<tr>
		<td>Kasir</td>
		<td>: <?= $head->nama_pegawai?></td>
	</tr>
</table>
<table width="100%">
	<tr>
		<td>Item</td>
		<td style="text-align: center;">Qty</td>
		<td style="text-align: right;">Harga</td>
		<td style="text-align: right;">Jumlah</td>
	</tr>
	<?php foreach($body as $rb){ ?>
		<tr>
			<td><?= $rb->nama_produk?></td>
			<td style="text-align: center;"><?= $rb->qty.' '.$rb->satuan?></td>
			<td style="text-align: right;"><?= number_format($rb->harga,0,'','.')?></td>
			<td style="text-align: right;"><?= number_format($rb->jumlah,0,'','.')?></td>
		</tr>
	<?php } ?>
	<tr>
		<td  style="text-align: right" colspan="3" >Total</td>
		<td  style="text-align: right"><?= number_format($head->jumlah,0,'','.')?></td>
	</tr>
</table>


