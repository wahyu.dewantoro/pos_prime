<div class="row">
            <div class="col-12">
                <div class="card-box">
                    <div class="row">
                        <div class="col-md-12 ">
                             <form action="<?php echo site_url('penjualan'); ?>" class="form-horizontal" method="get">
                                <div class="form-group row">
                                    <label class="col-1 col-form-label">Pencarian</label>
                                    <div class="col-4">
                                        <input type="text" class="form-control form-control-sm" name="q" value="<?php echo $q; ?>">
                                    </div>
                                    <label class="col-1 col-form-label">Printer</label>
                                    <div class="col-2">
                                        <input type="text" class="form-control form-control-sm" id="jenisprint" value="" readonly>
                                        <!-- <select class="form-control"  name="printer" id="printer">
                                        <option value="0">Semua</option>
                                        </select> -->
                                    </div>
                                    <div class="col-4">
                                        <?php  if ($q <> '') {   ?>
                                                <a href="<?php echo site_url('penjualan'); ?>" class="btn btn-warning"><i class="fa fa-refresh"></i> Reset</a>
                                        <?php   } ?>
                                        <button class="btn btn-primary" type="submit"><i class="fa fa-search"></i> Cari</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-bordered" style="margin-bottom: 10px">
                            <thead class="thead-light">
                            <tr>
                                <th>No</th>
                                <th>Kode Transaksi</th>
                                <th>Kasir</th>
                                <th>Pelanggan</th>
                                <th>Tanggal Transaksi</th>
                                <th>Jumlah</th>
                                <th>Diskon</th>
                                <th>Bayar</th>
                                <th></th>
                                </tr>
                                </thead>
                            <tbody>
                            <?php
                            foreach ($penjualan_data as $penjualan)
                            {
                                ?>
                                <tr>
                        			<td align="center" width="80px"><?php echo ++$start ?></td>
                        			<td><?php echo $penjualan->kode_transaksi ?></td>
                        			<td><?php echo $penjualan->nama_pegawai ?></td>
                        			<td><?php echo $penjualan->nama_pelanggan ?></td>
                        			<td><?php echo $penjualan->tanggal_transaksi ?></td>
                                    <td align="right"><?php echo number_format($penjualan->jumlah,0,'','.') ?></td>
                                    <td align="right"><?php echo number_format($penjualan->diskon,0,'','.') ?></td>
                                    <td align="right"><?php echo number_format($penjualan->jumlah_bayar,0,'','.') ?></td>
                        			<td style="text-align:center" width="100px">
                        			     <button data-id="" onclick="printStuff(<?= $penjualan->id?>)" class="badge badge-warning"><i class="fa fa-file"></i></button>
                                         <?php
                                        //  echo anchor(site_url('penjualan/cetakstruk3/'.acak($penjualan->id)),'<i class="fa fa-print"></i>','class="badge badge-success" data-toggle="tooltip" data-placement="top" title="" data-original-title="Cetak Struk"');
                                         echo anchor(site_url('penjualan/detail/'.acak($penjualan->id)),'<i class="fa fa-eye"></i>','class="badge badge-info" data-toggle="tooltip" data-placement="top" title="" data-original-title="Detail"');
                                        //  if($akses['is_update']==1){
                        				// echo anchor(site_url('penjualan/update/'.acak($penjualan->id)),'<i class="fa fa-edit"></i>','class="badge badge-info" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit data"'); }
                        				 if($akses['is_delete']==1){echo anchor(site_url('penjualan/delete/'.acak($penjualan->id)),'<i class="fa fa-trash"></i>','class="badge badge-danger" onclick="javasciprt: return confirm(\'Apakah anda yakin? data yang telah di hapus tidak dapat di kembalikan!\')" data-toggle="tooltip" data-placement="top" title="" data-original-title="Hapus data"'); }
                        				?>
                        			</td>
                        		</tr>
                                <?php
                            }
                            ?>
                            </tbody>
                        </table>
                        <div id="cetak"></div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <a href="#" class="btn btn-success">Total Data : <?php echo $total_rows ?></a>
	</div>
                        <div class="col-md-6 text-right">
                            <?php echo $pagination ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>