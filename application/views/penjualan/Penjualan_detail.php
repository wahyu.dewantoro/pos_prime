<style type="text/css">
    .inputkanan {
    text-align: right;
}
</style>
<div class="row">
    <div class="col-12">
        <div class="card-box">
            <form  method="post"  role="form" id="penjualan">
                <div class="row">
                   <div class="col-12">
                <div class="row">
                   <div class="col-6">
                   <table style="font-size: 12">
                        <tr>
                            <td>Tanggal</td>
                            <td>: <?= $head->tanggal_transaksi?></td>
                        </tr>
                        <tr>
                            <td>Kode</td>
                            <td>: <?= $head->kode_transaksi ?></td>
                        </tr>
                        <tr>
                            <td>Kasir</td>
                            <td>: <?= $head->nama_pegawai?></td>
                        </tr>
                    </table>
                    </div>
                   <div class="col-6">
                   <table style="font-size: 12">
                        <tr>
                            <td>Custumer</td>
                            <td>: <?= $pelanggan->nama_pelanggan?></td>
                        </tr>
                        <tr>
                            <td>No HP</td>
                            <td>: <?= $pelanggan->no_hp ?></td>
                        </tr>
                        <tr>
                            <td>Instansi</td>
                            <td>: <?= $pelanggan->instansi?></td>
                        </tr>
                        <tr>
                            <td>Email</td>
                            <td>: <?= $pelanggan->email?></td>
                        </tr>
                    </table>
                    </div>
                    </div>
                        <table class="table table-bordered" style="margin-bottom: 10px">
                        <thead class="thead-light">
                        <tr>
                            <td>Item</td>
                            <td style="text-align: center;">Qty</td>
                            <td style="text-align: right;">Harga</td>
                            <td style="text-align: right;">Jumlah</td>
                        </tr>
                        </thead>
                        <?php foreach($body as $rb){ ?>
                            <tr>
                                <td><?= $rb->nama_produk?></td>
                                <td style="text-align: center;"><?= $rb->qty.' '.$rb->satuan?></td>
                                <td style="text-align: right;"><?= number_format($rb->harga,0,'','.')?></td>
                                <td style="text-align: right;"><?= number_format($rb->jumlah,0,'','.')?></td>
                            </tr>
                        <?php } ?>
                        <tr>
                            <td  style="text-align: right" colspan="3" >Jumlah</td>
                            <td  style="text-align: right"><?= number_format($head->jumlah,0,'','.')?></td>
                        </tr>
                        <tr>
                            <td  style="text-align: right" colspan="3" >Diskon</td>
                            <td  style="text-align: right"><?= number_format($head->diskon,0,'','.')?></td>
                        </tr>
                        <tr>
                            <td  style="text-align: right" colspan="3" >Total</td>
                            <td  style="text-align: right"><?= number_format($head->jumlah_bayar,0,'','.')?></td>
                        </tr>
                    </table>
                   </div>
                </div>





                       <input type="hidden" name="id" value="<?php echo $id; ?>" />


            </form>
        </div>
    </div>
</div>
