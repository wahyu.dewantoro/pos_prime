<div class="row">
    <div class="col-12">
        <div class="card-box">
            <form action="<?php echo $action; ?>" method="post"  role="form">
                <div class="row">
                    
                        <input type ="hidden" readonly class="form-control"  name="nip" id="nip" placeholder="NIP" value="<?php echo $nip; ?>">
                        <input type ="hidden" readonly class="form-control"  name="nama_pegawai" id="nama_pegawai" placeholder="Nama Pegawai" value="<?php echo $nama_pegawai; ?>">
                        <div class="col-md-6"> 
                                <div class="form-group row">
                                    <label class="col-3 col-form-label">Nama Custumer <sup style="color: red"></sup></label>
                                    <div class="col-9">
                                        <input type="text" name="nama_pelanggan" id="nama_pelanggan" class="form-control">
                                        <?= form_error('nama_pelanggan'); ?>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-3 col-form-label">Instansi </label>
                                    <div class="col-9">
                                        <input type="text" name="instansi" id="instansi" class="form-control">
                                        
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-3 col-form-label">Email </label>
                                    <div class="col-9">
                                        <input type="email" name="email" id="email" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-3 col-form-label">No HP </label>
                                    <div class="col-9">
                                        <input type="text" name="no_hp" id="no_hp" class="form-control">
                                    </div>
                                </div>
                        </div>
                        <div class="col-md-6">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>Produk</th>
                                        <!-- <th>Satuan</th> -->
                                        <th>Qty</th>
                                        <th>Harga</th>
                                        <th>Jumlah</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php for($i=0; $i<=3;$i++){?>
                                    <tr>
                                        <td>
                                            <div class="input-group">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text carinip" data-valid="<?= $i ?>" id="basic-addon1"><i class="fa fa-search"></i></span>
                                                </div>
                                                <input type="text" readonly="" name="nama_produk[]" id="nama_produk<?= $i ?>" class="form-control">
                                            </div>
                                            <input type="hidden" name="pos_produk_id[]" id="pos_produk_id<?= $i ?>">
                                            <input type="hidden" name="satuan[]" id="satuan<?= $i ?>" class="form-control">
                                        </td>
                                        <td>
                                            <input type="text" name="qty[]" id="qty<?= $i ?>" class="form-control">
                                        </td>
                                        <td>
                                            <div class="input-group">
                                                

                                                <input type="text" readonly="" name="harga[]" id="harga<?= $i ?>" class="form-control">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text"><span id="vsatuan<?= $i ?>"></span></span>
                                                </div>
                                            </div>
                                            
                                        </td>
                                        <td>
                                            <!-- <input type="text" name="jumlah[]" id="jumlah<?= $i ?>" class="form-control"> -->
                                        </td>
                                    </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                            <button type="submit" class="btn btn-success waves-effect waves-light"><i class="fa fa-save"></i> Submit</button>
                                <button type="reset" class="btn btn-warning waves-effect waves-light"><i class="fa fa-refresh"></i> Reset</button>
                        </div>
                     
                       <input type="hidden" name="id" value="<?php echo $id; ?>" /> 
                       

                    </form>
                </div>
            </div>
        </div>

<!-- Modal -->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog" style="width:1000px">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">List Pegawai</h4>
                </div>
                <div class="modal-body">
                    <table id="lookup" class="table table-bordered table-hover table-striped">
                        <thead>
                            <tr>
                                  <th>No</th>
                                  <th>Nama Produk</th>
                                  <th>Harga</th>
                                  <th>Satuan
                                    <input type="hidden" name="valid" id="valid" class="valid">
                                  </th>
                            </tr>
                        </thead>
                        <tbody>
                              <?php $start=1;
                        foreach ($produk as $produk)
                        {
                            ?>
                            <tr class="pilih" data-harga="<?= $produk->harga ?>" data-pos_id="<?= $produk->id ?>" data-nama_produk="<?php echo $produk->nama_produk ?>" data-satuan="<?php echo $produk->satuan ?>">

                                    <td align="center" width="80px"><?php echo $start++ ?> </td>
                                    <td><?php echo $produk->nama_produk ?></td>
                                    <td align="right"><?php echo ribuan($produk->harga) ?></td>
                                    <td><?php echo $produk->satuan ?></td>
                            </tr>
                            <?php
                        }
                        ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>