<!DOCTYPE html>
<html>
<head>
	<title>Cetak Tabungan Printer Thermal</title>
	<!DOCTYPE html>
<html>
	<style type="text/css">

@page {
	size: 8.5in 11in; /* <length>{1,2} | auto | portrait | landscape */
	      /* 'em' 'ex' and % are not allowed; length values are width height */
	margin: 1%; /* <any of the usual CSS values for margins> */
	             /*(% of page-box width for LR, of height for TB) */
	margin-header: 1mm; /* <any of the usual CSS values for margins> */
	margin-footer: 1mm; /* <any of the usual CSS values for margins> */
	marks: /*crop | cross | none*/
	/* header: html_myHTMLHeaderOdd;
	footer: html_myHTMLFooterOdd; */
	background: ...
	background-image: ...
	background-position ...
	background-repeat ...
	background-color ...
	background-gradient: ...
}
body {
	margin:10px 0px;
	padding:0px;
	text-align:center;
}

.konten {
	width:200px;
	/*margin:0px auto; */
	/*text-align:left; */
	/*padding:5px; */
}

.header {
	text-align: center;
}

td{
	font-family: Arial;
	font-size: 8pt;
	padding: 4px;
	color:black;
	vertical-align: top;
}

.tabel{
	width: 100%;
	border-collapse: collapse;
	padding: 10px;
}

.tabel-stafel{
	width: 85%;
	border-collapse: collapse;
	padding: 15px;
}

.garis_bawah{
	border-collapse: collapse;
	border-bottom:1px solid #000;
}

.garis_atas{
	border-collapse: collapse;
	border-top:1px solid #000;
}

.total{
	border-top:2px solid #000;
}

.tebal{
	font-weight: bold;
}

.miring{
	font-style: italic;
}

.alamat{
	font-family: Arial;
	font-size: 6pt;
	padding: 0px;
	margin: 0px 0px;
}

.judul1{
	font-size: 3pt;
	margin-top: 0px;
	margin-bottom: 0px;
}

.judul2{
	font-size: 3pt;
	margin-top: 0px;
	margin-bottom: 0px;
	font-weight: bold;
}

th {
	font-family: Arial;
	font-size: 3pt;
	padding: 5px;
	vertical-align: middle;
	border-color:black;
	background-color:black;
	color:white;
}
td {
	font-family: Arial;
	font-size: 3pt;
}
p {
	font-family: Arial;
	font-size: 3pt;
}

h1 {
	font-family: Arial;
	font-size: 4pt;
	padding: 0px;
	margin: 5px 0px;
}

h2 {
	font-family: "Trajan Pro", Garamond;
	font-size: 4pt;
	padding: 0px;
	margin: 5px 0px;
}

h3 {
	font-family: Arial;
	font-size: 4pt;
	padding: 0px;
	margin: 3px 0px;
}

hr {
	border-color: #000000;
}
	</style>
</head>
<body>
  <table width="80px">
    <tr>
      <td>
      	<center>
				<img src="<?= base_url() ?>/assets/logo.png" alt=""width="20" height="20" >
        <p class="judul2">PRIME Digital Printing</p>
        <p class="judul1">The best solutions for printing</p><br>
      	</center>
        Tanggal: <?= $head->tanggal_transaksi?> WIB<br>
        Kasir: <?= $head->nama_pegawai?><br>
		Kode:	<?= $head->kode_transaksi ?><br>
      </td>
    </tr>
  </table>
  <table width="80px" style="border-collapse: collapse;">
    <tbody>

	<?php foreach($body as $rb){ ?>
		<tr>
			<td ><?= $rb->nama_produk?> x <?= $rb->qty.' '.$rb->satuan?></td>
			<td style="text-align: right;"><?= number_format($rb->jumlah,0,'','.')?></td>
		</tr>
	<?php } ?>

	<tr>
		<td  class="garis_bawah garis_atas" style="text-align: left" colspan="1" >Jumlah</td>
		<td  class="garis_bawah garis_atas" style="text-align: right"><?= number_format($head->jumlah,0,'','.')?></td>
	</tr>
	<tr>
		<td class="garis_bawah garis_atas" style="text-align: left" colspan="1" >Diskon</td>
		<td class="garis_bawah garis_atas" style="text-align: right"><?= number_format($head->diskon,0,'','.')?></td>
	</tr>
	<tr>
		<td  style="text-align: left" colspan="1" >Total</td>
		<td  style="text-align: right"><?= number_format($head->jumlah-$head->diskon,0,'','.')?></td>
	</tr>
	<tr>
		<td colspan="2" align="center">Terim Kasih</td>
	</tr>
	<tr>
		<td colspan="2" align="center"><?= date("Y-m-d H:i:s")?></td>
	</tr>
    </tbody>
  </table>
  <!-- <div style='page-break-before: always'> -->
</body>
</html>