<!-- <script src="http://code.jquery.com/jquery-2.1.4.min.js"></script> -->
<div class="row">
            <div class="col-12">
                <div class="card-box">
                    <form action="<?php echo $action; ?>" method="post"  class="form-horizontal" role="form">

                        <div class="form-group row">
                            <label for="tgl" class="col-3 col-form-label">Tanggal Pembelian <sup style="color:red;">*</sup></label>
                            <div class="col-3">
                                <input type="date" class="form-control"  name="tgl" id="tgl" placeholder="Tanggal Pembelian" value="<?php echo $tgl; ?>">
                                <?php echo form_error('tgl') ?>
                            </div>
                            <label for="jumlah" class="col-2 col-form-label">Jumlah <sup style="color:red;">*</sup></label>
                            <div class="col-3">
                                <input onkeyup="formatangka(this);" type="text" class="form-control" onchange="hitung()"  name="jumlah" id="jumlah" placeholder="Jumlah" value="<?php echo $jumlah; ?>">
                                <?php echo form_error('jumlah') ?>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="hp" class="col-3 col-form-label">Nama Supplier <sup style="color:red;">*</sup></label>
                            <div class="col-3">
                            <select class="form-control select2" name="supplier" id="supplier" onchange="getsup(this);">
                                <option value="">Supplier</option>
                                <?php foreach ($supplier_list as $rk) { ?>
                                    <option <?php if($rk->id==$sup_id) { echo "selected";} ?> value="<?php echo $rk->id; ?>"><?php echo $rk->nama; ?></option>
                                <?php } ?>
                            </select>
                            <input type="hidden" name="nama_supplier" id="nama_supplier" value="<?php echo $sup_nama ?>">
                                <?php echo form_error('supplier') ?>
                            </div>
                            <label for="harga" class="col-2 col-form-label">Harga <sup style="color:red;">*</sup></label>
                            <div class="col-3">
                                <input onkeyup="formatangka(this);" type="text" class="form-control" onchange="hitung()"  name="harga" id="harga" placeholder="Harga" value="<?php echo $harga; ?>">
                                <?php echo form_error('harga') ?>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="kd_barang" class="col-3 col-form-label">Kode Barang <sup style="color:red;">*</sup></label>
                            <div class="col-3">
                                <!-- <input type="text" class="form-control"  name="kd_barang" id="kd_barang" placeholder="Kode Barang" value="<?php echo $kd_barang; ?>"> -->
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text cariproduk" id="basic-addon1"><i class="fa fa-search"></i></span>
                                    </div>
                                    <input readonly type="text" class="form-control cariproduk"  name="kd_barang" id="kd_barang" placeholder="Kode Barang" value="<?php echo $kd_barang; ?>">

                                </div>
                                <?php echo form_error('kd_barang') ?>
                            </div>
                            <label for="total" class="col-2 col-form-label">Total Pembelian <sup style="color:red;">*</sup></label>
                            <div class="col-3">
                                <input readonly type="text" class="form-control"  name="total" id="total" placeholder="Total Pembelian" value="<?php echo $total; ?>">
                                <?php echo form_error('total') ?>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="nama_barang" class="col-3 col-form-label">Nama Barang <sup style="color:red;">*</sup></label>
                            <div class="col-3">
                                <input type="text" readonly class="form-control"  name="nama_barang" id="nama_barang" placeholder="Nama Barang" value="<?php echo $nama_barang; ?>">
                                <?php echo form_error('nama_barang') ?>
                            </div>
                        </div>
                        <input type="hidden" name="id" value="<?= $id ?>">
	                   <div class="form-group mb-0 justify-content-end row">
                            <div class="col-9">
                                <button type="submit" class="btn btn-success waves-effect waves-light"><i class="fa fa-save"></i> Submit</button>
                                <button type="reset" class="btn btn-warning waves-effect waves-light"><i class="fa fa-refresh"></i> Reset</button>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
        <!-- Modal -->
        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog" style="width:800px">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">List Barang</h4>
                    </div>
                    <div class="modal-body">
                        <table id="lookup" class="table table-bordered table-hover table-striped">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                <th>Nama Barang</th>
                                <th>harga</th>
                                <th>Satuan</th>
                                </tr>
                            </thead>
                            <tbody>
                                  <?php
                            foreach ($produk as $produk)
                            {
                                ?>
                                <tr class="pilih" data-produk="<?php echo $produk->id ?>" data-nama_produk="<?php echo $produk->nama_produk ?>">
                                    <td><?php echo $produk->id ?></td>
                                    <td><?php echo $produk->nama_produk ?></td>
                                    <td><?php echo $produk->harga ?></td>
                                    <td><?php echo $produk->satuan ?></td>
                                </tr>
                                <?php
                            }
                            ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
<script>
     function formatangka(objek) {
     a = objek.value;
     b = a.replace(/[^\d]/g,"");
     c = "";
     panjang = b.length;
     j = 0;
     for (i = panjang; i > 0; i--) {
       j = j + 1;
       if (((j % 3) == 1) && (j != 1)) {
         c = b.substr(i-1,1) + "." + c;
       } else {
         c = b.substr(i-1,1) + c;
       }
     }
     objek.value = c;
  };
function hitung() {
    var harga =$("#harga").val();
    var jumlah =$("#jumlah").val();
    if(harga==''||isNaN(harga)){
        harga=0
    }
    if(jumlah==''||isNaN(jumlah)){
        jumlah=0
    }

    var total = gantiTitikKoma(jumlah)*gantiTitikKoma(harga);
    $("#total").val(numberWithCommas(total));
}
function gantiTitikKoma(angka){
         var angka= "" +angka;
      return angka.toString().replace(/\./g,'');
    }
    function numberWithCommas(x) {
         var x= "" +x;
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
}
function getsup(sel)
{
    var text = $("#supplier option:selected").text();
    $("#nama_supplier").val(text);
}
</script>