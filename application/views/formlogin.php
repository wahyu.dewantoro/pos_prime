<!doctype html>
<html lang="en">
    
<!-- Mirrored from coderthemes.com/highdmin/horizontal/page-login.html by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 06 Jan 2019 08:32:45 GMT -->
<head>
        <meta charset="utf-8" />
        <title>Login</title>
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta content="A fully featured admin theme which can be used to build CRM, CMS, etc." name="description" />
        <meta content="Coderthemes" name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />

        <!-- App favicon -->
        <link rel="shortcut icon" href="<?= base_url() ?>highdmin/horizontal/assets/images/favicon.ico">

        <!-- App css -->
        <link href="<?= base_url() ?>highdmin/horizontal/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="<?= base_url() ?>highdmin/horizontal/assets/css/icons.css" rel="stylesheet" type="text/css" />
        <link href="<?= base_url() ?>highdmin/horizontal/assets/css/style.css" rel="stylesheet" type="text/css" />

        <script src="<?= base_url() ?>highdmin/horizontal/assets/js/modernizr.min.js"></script>

    </head>

    <body>

        <!-- Begin page -->
        <div class="accountbg" style="background: url('assets/images/bg-1.jpg');background-size: cover;background-position: center;"></div>

        <div class="wrapper-page account-page-full">

            <div class="card">
                <div class="card-block">

                    <div class="account-box">

                        <div class="card-box p-5">
                            <h2 class="text-uppercase text-center pb-4">
                                <a href="#" class="text-success">
                                    <span><img src="<?= base_url() ?>highdmin/horizontal/assets/images/logo.png" alt="" width="100"></span>
                                </a>
                            </h2>
                            <form class="" action="<?= base_url().'auth/proses'?>" autocomplate="off" method="post">
                                 <input id="username" style="display:none" type="text" name="fakeusernameremembered">
                                 <input id="password" style="display:none" type="password" name="fakepasswordremembered">
                                <div class="form-group m-b-20 row">
                                    <div class="col-12">
                                        <label for="emailaddress">Username</label>
                                        <input class="form-control" type="text" id="real_username" name="real_username" required="" placeholder="Masukan username" autocomplete="nope">
                                    </div>
                                </div>
                                <div class="form-group row m-b-20">
                                    <div class="col-12">
                                        <label for="password">Password</label>
                                        <input class="form-control" type="password" required="" name="real_password" id="real_password" placeholder="Masukan Password" autocomplete="new-password">
                                    </div>
                                </div>
                                <div class="form-group row text-center m-t-10">
                                    <div class="col-12">
                                        <button class="btn btn-block btn-custom waves-effect waves-light" type="submit">Sign In</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>

                </div>
            </div>

            <div class="m-t-40 text-center">
                <p class="account-copyright">2019 © W.O.B Production</p>
            </div>

        </div>


        <!-- jQuery  -->
        <script src="<?= base_url() ?>highdmin/horizontal/assets/js/jquery.min.js"></script>
        <script src="<?= base_url() ?>highdmin/horizontal/assets/js/bootstrap.bundle.min.js"></script>
        <script src="<?= base_url() ?>highdmin/horizontal/assets/js/waves.js"></script>
        <script src="<?= base_url() ?>highdmin/horizontal/assets/js/jquery.slimscroll.js"></script>

        <!-- App js -->
        <script src="<?= base_url() ?>highdmin/horizontal/assets/js/jquery.core.js"></script>
        <script src="<?= base_url() ?>highdmin/horizontal/assets/js/jquery.app.js"></script>

    </body>


</html>