<div class="row">
            <div class="col-12">
                <div class="card-box">
                    <div class="row">
                        <div class="col-md-12 ">
                             <form action="<?php echo site_url('lap_penjualan'); ?>" class="form-horizontal" method="get">
                                <div class="form-group row">
                                    <label class="col-1 col-form-label">Tanggal 1</label>
                                    <div style="width:200px">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" id="basic-addon1"><i class="mdi mdi-calendar"></i></span>
                                            </div>
                                            <input type="date" class="form-control"  name="date1" id="date1" placeholder="Tanggal 1" value="<?php echo $date1; ?>">
                                        </div>
                                    </div>
                                    <label class="col-1 col-form-label">Tanggal 2</label>
                                    <div style="width:200px">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" id="basic-addon1"><i class="mdi mdi-calendar"></i></span>
                                            </div>
                                            <input type="date" class="form-control"  name="date2" id="date2" placeholder="Tanggal 2" value="<?php echo $date2; ?>">
                                        </div>
                                    </div>
                                    <div class="col-5">
                                        <button class="btn btn-primary" type="submit"><i class="fa fa-search"></i> Cari</button>
                                        <a href="<?php echo $cetak ?>" class="btn btn-success"><i class="mdi mdi-file-excel"></i> Cetak</a>
                                        <a href="<?php echo $cetak2 ?>" class="btn btn-success"><i class="mdi mdi-file-excel"></i> Cetak Detail</a>
                                        <?php  if ($date1 <> '' || $date2 <> '') {   ?>
                                                <a href="<?php echo site_url('lap_penjualan'); ?>" class="btn btn-warning"><i class="fa fa-refresh"></i> Reset</a>
                                        <?php   } ?>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-bordered" style="margin-bottom: 10px">
                            <thead class="thead-light">
                            <tr>
                                <th>No</th>
                                <th>Kode Transaksi</th>
                                <th>Kasir</th>
                                <th>Pelanggan</th>
                                <th>Tanggal Transaksi</th>
                                <th>Jumlah</th>
                                <th>Diskon</th>
                                <th>Bayar</th>
                                </tr>
                                </thead>
                            <tbody>
                            <?php
                            foreach ($penjualan_data as $penjualan)
                            {
                                ?>
                                <tr>
                        			<td align="center" width="80px"><?php echo ++$start ?></td>
                        			<td><?php echo $penjualan->kode_transaksi ?></td>
                        			<td><?php echo $penjualan->nama_pegawai ?></td>
                        			<td><?php echo $penjualan->nama_pelanggan ?></td>
                        			<td><?php echo $penjualan->tanggal_transaksi ?></td>
                                    <td align="right"><?php echo number_format($penjualan->jumlah,0,'','.') ?></td>
                                    <td align="right"><?php echo number_format($penjualan->diskon,0,'','.') ?></td>
                                    <td align="right"><?php echo number_format($penjualan->jumlah_bayar,0,'','.') ?></td>
                        		</tr>
                                <?php
                            }
                            ?>
                            </tbody>
                        </table>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <a href="#" class="btn btn-success">Total Data : <?php echo $total_rows ?></a>
	</div>
                        <div class="col-md-6 text-right">
                            <?php echo $pagination ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>