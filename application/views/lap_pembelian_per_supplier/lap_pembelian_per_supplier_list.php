<div class="row">
            <div class="col-12">
                <div class="card-box">
                    <div class="row">
                        <div class="col-md-12 ">
                             <form action="<?php echo site_url('lap_pembelian_per_supplier'); ?>" class="form-horizontal" method="get">
                                <div class="form-group row">
                                    <label class="col-1 col-form-label">Supplier</label>
                                    <div class="col-3">
                                    <select class="form-control select2" name="pos_supplier_id" id="pos_supplier_id" >
                                        <option value="">Supplier</option>
                                        <?php foreach ($supplier_list as $rk) { ?>
                                            <option <?php if($rk->id==$pos_supplier_id) { echo "selected";} ?> value="<?php echo $rk->id; ?>"><?php echo $rk->nama; ?></option>
                                        <?php } ?>
                                    </select>
                                    </div>
                                    <div class="col-4">
                                        <button class="btn btn-primary" type="submit"><i class="fa fa-search"></i> Cari</button>
                                        <a href="<?php echo $cetak ?>" class="btn btn-success"><i class="mdi mdi-file-excel"></i> Cetak</a>
                                        <?php  if ($pos_supplier_id <> '') {   ?>
                                                <a href="<?php echo site_url('lap_pembelian_per_supplier'); ?>" class="btn btn-warning"><i class="fa fa-refresh"></i> Reset</a>

                                        <?php   } ?>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-bordered" style="margin-bottom: 10px">
                            <thead class="thead-light">
                            <tr>
                                <th>No</th>
                                <th>Tgl Pembelian</th>
                                <th>Nama Supplier</th>
                                <th>Nama Petugas</th>
                                <th>Kode Barang</th>
                                <th>Nama Barang</th>
                                <th>Jumlah</th>
                                <th>Harga</th>
                                <th>Total Pembelian</th>
                                </tr>
                                </thead>
                            <tbody>
                            <?php
                            foreach ($pembelian_data as $rk)
                            {
                                ?>
                                <tr>
                        			<td align="center" width="80px"><?php echo ++$start ?></td>
                        			<td><?php echo date("d-m-Y", strtotime($rk->tgl_pembelian)); ?></td>
                        			<td><?php echo $rk->nama_suplier ?></td>
                        			<td><?php echo $rk->nama_pegawai ?></td>
                        			<td><?php echo $rk->kd_barang ?></td>
                        			<td><?php echo $rk->nama_barang ?></td>
                        			<td class="text-right"><?php echo number_format($rk->qty_pembelian,0,",","."); ?></td>
                        			<td class="text-right"><?php echo number_format($rk->harga_per_item,0,",","."); ?></td>
                        			<td class="text-right"><?php echo number_format($rk->harga_jumlah,0,",","."); ?></td>
                        		</tr>
                                <?php
                            }
                            ?>
                            </tbody>
                        </table>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <a href="#" class="btn btn-success">Total Data : <?php echo $total_rows ?></a>
	</div>
                        <div class="col-md-6 text-right">
                            <?php echo $pagination ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>