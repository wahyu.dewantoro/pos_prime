<?php $namabulan=array(
      '',
      'Januari',
      'Februari',
      'Maret',
      'April',
      'Mei',
      'Juni',
      'Juli',
      'Agustus',
      'September',
      'Oktober',
      'November',
      'Desember'
      ) ?>
<div class="row">
            <div class="col-12">
                <div class="card-box">
                    <div class="row">
                        <div class="col-md-12 ">
                             <form action="<?php echo site_url('lap_penggajian'); ?>" class="form-horizontal" method="get">
                                <div class="form-group row">
                                    <label class="col-1 col-form-label">Bulan</label>
                                    <div class="col-2">
                                    <select id="bulan_gaji"  name="bulan_gaji" required="required" placeholder="Bulan Gaji" class="form-control select2 col-md-7 col-xs-12">
                                        <option value="">Pilih</option>
                                        <?php for ($i=1; $i < 13 ; $i++) { ?>
                                        <option <?php if($i==$bulan_gaji){echo "selected";}?>  value="<?php echo $i; ?>"><?php echo $namabulan[$i] ?></option>
                                        <?php } ?>
                                    </select>
                                    </div>
                                    <label class="col-1 col-form-label">Tahun</label>
                                    <div class="col-2">
                                    <select id="tahun_gaji"  name="tahun_gaji" required="required" placeholder="Bulan Gaji" class="form-control select2 col-md-7 col-xs-12">
                                        <option value="">Pilih</option>
                                        <?php for ($i=2019; $i < 2025 ; $i++) { ?>
                                        <option <?php if(!empty($tahun_gaji)){ if($i==$tahun_gaji){echo "selected";}} else {if($i==date("Y")){echo "selected";}}?>  value="<?php echo $i; ?>"><?php echo $i?></option>
                                        <?php } ?>
                                    </select>
                                    </div>
                                    <div class="col-4">
                                        <button class="btn btn-primary" type="submit"><i class="fa fa-search"></i> Cari</button>
                                        <a href="<?php echo $cetak ?>" class="btn btn-success"><i class="mdi mdi-file-excel"></i> Cetak</a>
                                        <?php  if ($bulan_gaji <> '' || $tahun_gaji <> '') {   ?>
                                                <a href="<?php echo site_url('lap_penggajian'); ?>" class="btn btn-warning"><i class="fa fa-refresh"></i> Reset</a>
                                        <?php   } ?>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-bordered" style="margin-bottom: 10px">
                            <thead class="thead-light">
                            <tr>
                                <th>No</th>
                                <th>Tanggal Transaksi</th>
                                <th>Bulan Gaji</th>
                                <th>Tahun Gaji</th>
                                <th>Nip Pegawai</th>
                                <th>Nama Pegawai</th>
                                <th>Gaji Pokok</th>
                                <th>Tunjangan</th>
                                <th>Bonus</th>
                                </tr>
                                </thead>
                            <tbody>
                            <?php
                            foreach ($penggajian_data as $penggajian)
                            {
                                ?>
                                <tr>
                                <td align="center" width="80px"><?php echo ++$start ?></td>
                                <td><?php echo date_indo($penggajian->tanggal_transaksi) ?></td>
                                <td><?php echo $namabulan[$penggajian->bulan_gaji] ?></td>
                                <td class="text-right"><?php echo $penggajian->tahun_gaji ?></td>
                                <td class="text-right"><?php echo $penggajian->nip_pegawai ?></td>
                                <td><?php echo $penggajian->nama_pegawai ?></td>
                                <td class="text-right"><?php echo number_format($penggajian->gaji_pokok,0,",",".") ?></td>
                                <td class="text-right"><?php echo number_format($penggajian->tunjangan,0,",",".") ?></td>
                                <td class="text-right"><?php echo number_format($penggajian->bonus,0,",",".") ?></td>
                            </tr>
                                <?php
                            }
                            ?>
                            </tbody>
                        </table>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <a href="#" class="btn btn-success">Total Data : <?php echo $total_rows ?></a>
	</div>
                        <div class="col-md-6 text-right">
                            <?php echo $pagination ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>