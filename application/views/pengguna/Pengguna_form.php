<div class="row">
            <div class="col-12">
                <div class="card-box">
                    <form action="<?php echo $action; ?>" method="post"  class="form-horizontal" role="form">
	                   <div class="row">
                           <div class="col-6">

                                 <div class="form-group row">
                                    <label for="nip" class="col-3 col-form-label">NIP <sup style="color:red;">*</sup></label>
                                    <div class="col-9">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text carinip" id="basic-addon1"><i class="fa fa-search"></i></span>
                                            </div>
                                            <input readonly type="text" class="form-control carinip"  name="nip" id="nip" placeholder="NIP Pegawai" value="<?php echo $nip; ?>">

                                        </div>


                                        <?php echo form_error('nip') ?>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="nama_lengkap" class="col-3 col-form-label">Nama Lengkap <sup style="color:red;">*</sup></label>
                                    <div class="col-9">
                                        <input type="text" class="form-control" readonly name="nama_lengkap" id="nama_lengkap" placeholder="Nama Lengkap" value="<?php echo $nama_lengkap; ?>">
                                        <?php echo form_error('nama_lengkap') ?>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="no_telepon" class="col-3 col-form-label">No Telepon <sup style="color:red;">*</sup></label>
                                    <div class="col-9">
                                        <input type="number" class="form-control"  readonly name="no_telepon" id="no_telepon" placeholder="No Telepon" value="<?php echo $no_telepon; ?>">
                                        <?php echo form_error('no_telepon') ?>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="email" class="col-3 col-form-label">Email <sup style="color:red;">*</sup></label>
                                    <div class="col-9">
                                        <input type="text" class="form-control"  name="email" id="email" placeholder="Email" value="<?php echo $email; ?>">
                                        <?php echo form_error('email') ?>
                                    </div>
                                </div>
                           </div>
                           <div class="col-6">
                                  <?php if($nambah==true){?>
                                  <div class="form-group row">
                                        <label for="username" class="col-3 col-form-label">Username <sup style="color:red;">*</sup></label>
                                        <div class="col-9">
                                            <input type="text" class="form-control"  name="username" id="username" placeholder="Username" value="<?php echo $username; ?>">
                                            <?php echo form_error('username') ?>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="password" class="col-3 col-form-label">Password <sup style="color:red;">*</sup></label>
                                        <div class="col-9">
                                            <input type="password" class="form-control"  name="password" id="password" placeholder="Password" value="<?php echo $password; ?>">
                                            <?php echo form_error('password') ?>
                                        </div>
                                    </div>

                                <?php } ?>

                                    <div class="form-group row">
                                        <label for="password" class="col-3 col-form-label">Role <sup style="color:red;">*</sup></label>
                                        <div class="col-9">
                                            <?php $a=1; foreach($role as $role){?>
                                                 <div class="custom-control custom-checkbox">
                                                    <input type="checkbox"  <?php if(in_array($role->id_inc, $assign) ){echo "checked";}?> class="custom-control-input" id="customCheck<?= $a; ?>" name="ms_role_id[]" value="<?= $role->id_inc ?>">
                                                    <label class="custom-control-label" for="customCheck<?= $a; ?>"><?= $role->nama_role ?></label>
                                                </div>

                                          <?php $a++; } ?>
                                        </div>
                                    </div>


                                    <input type="hidden" name="id_inc" value="<?= $id_inc ?>">
                                    <div class="form-group mb-0 justify-content-end row">
                                        <div class="col-9">
                                            <button type="submit" class="btn btn-success waves-effect waves-light"><i class="fa fa-save"></i> Submit</button>
                                            <button type="reset" class="btn btn-warning waves-effect waves-light"><i class="fa fa-refresh"></i> Reset</button>
                                        </div>
                                    </div>
                            </div>
                       </div>

                    </form>
                </div>
            </div>
        </div>

  <!-- Modal -->
        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog" style="width:800px">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">List Pegawai</h4>
                    </div>
                    <div class="modal-body">
                        <table id="lookup" class="table table-bordered table-hover table-striped">
                            <thead>
                                <tr>
                                    <th>NIP</th>
                                    <th>Nama</th>
                                    <th>Hp</th>
                                    <th>Mulai Kerja</th>
                                </tr>
                            </thead>
                            <tbody>
                                  <?php
                            foreach ($pegawai as $pegawai)
                            {
                                ?>
                                <tr class="pilih" data-no_telepon="<?php echo $pegawai->hp ?>" data-nip="<?php echo $pegawai->nip ?>" data-nama_pegawai="<?php echo $pegawai->nama ?>">
                                    <td><?php echo $pegawai->nip ?></td>
                                    <td><?php echo $pegawai->nama ?></td>
                                    <td><?php echo $pegawai->hp ?></td>
                                    <td><?php echo date_indo($pegawai->tgl_mulai_kerja) ?></td>
                                </tr>
                                <?php
                            }
                            ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>