<script>
    $(document).on('click', '.reset', function (e) {
        $('#idx').val($(this).attr("data-id"));
        $('#nama_lengkap').val($(this).attr("data-nama_lengkap"));
        $('#no_telepon').val($(this).attr("data-no_telepon"));
        $('#email').val($(this).attr("data-email"));
        $('#username').val($(this).attr("data-username"));
        $('#role').val($(this).attr("data-role"));
        $('#myModal').modal('show');

    });
</script>