<div class="row">
    <div class="col-12">
        <div class="card-box">
            <div class="row">
                <div class="col-md-12 ">
                    <form action="<?php echo site_url('pengguna'); ?>" class="form-horizontal" method="get">
                        <div class="form-group row">
                            <label class="col-1 col-form-label">Pencarian</label>
                            <div class="col-4">
                                <input type="text" class="form-control form-control-sm" name="q"
                                    value="<?php echo $q; ?>">
                            </div>
                            <div class="col-4">
                                <?php  if ($q <> '') {   ?>
                                <a href="<?php echo site_url('pengguna'); ?>" class="btn btn-warning"><i
                                        class="fa fa-refresh"></i> Reset</a>
                                <?php   } ?>
                                <button class="btn btn-primary" type="submit"><i class="fa fa-search"></i> Cari</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="table-responsive">
                <table class="table table-bordered" style="margin-bottom: 10px">
                    <thead class="thead-light">
                        <tr>
                            <th>No</th>
                            <th>Nama Lengkap</th>
                            <th>No Telepon</th>
                            <th>Email</th>
                            <th>Username</th>
                            <th>Role</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                            foreach ($pengguna_data as $pengguna)
                            {
                                ?>
                        <tr>
                            <td align="center" width="80px"><?php echo ++$start ?></td>
                            <td><?php echo $pengguna->nama_lengkap ?></td>
                            <td><?php echo $pengguna->no_telepon ?></td>
                            <td><?php echo $pengguna->email ?></td>
                            <td><?php echo $pengguna->username ?></td>
                            <td><?php echo $pengguna->role?></td>
                            <td style="text-align:center" width="100px">
                                <a href="#" class="badge badge-warning reset" data-id="<?=acak($pengguna->id_inc)?>"
                                    data-nama_lengkap="<?=$pengguna->nama_lengkap?>"
                                    data-no_telepon="<?=$pengguna->no_telepon?>" data-email="<?=$pengguna->email?>"
                                    data-username="<?=$pengguna->username?>" data-role="<?=$pengguna->role?>"
                                    data-toggle="tooltip" data-placement="top" title=""
                                    data-original-title="Reset Password" aria-describedby="tooltip430364"><i
                                        class="fa fa-refresh"></i></a>
                                <?php

                                        if($akses['is_update']==1){
                        				echo anchor(site_url('pengguna/update/'.acak($pengguna->id_inc)),'<i class="fa fa-edit"></i>','class="badge badge-info" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit data"'); }
                        				 if($akses['is_delete']==1){echo anchor(site_url('pengguna/delete/'.acak($pengguna->id_inc)),'<i class="fa fa-trash"></i>','class="badge badge-danger" onclick="javasciprt: return confirm(\'Apakah anda yakin? data yang telah di hapus tidak dapat di kembalikan!\')" data-toggle="tooltip" data-placement="top" title="" data-original-title="Hapus data"'); }
                        				?>
                            </td>
                        </tr>
                        <?php
                            }
                            ?>
                    </tbody>
                </table>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <a href="#" class="btn btn-success">Total Data : <?php echo $total_rows ?></a>
                </div>
                <div class="col-md-6 text-right">
                    <?php echo $pagination ?>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" style="width:800px">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Form Reset Password</h4>
            </div>
            <div class="modal-body">

                <form action="<?php echo site_url('pengguna/reset') ?>" method="post" class="form-horizontal"
                    role="form">
                    <div class="form-group row">
                        <label for="password" class="col-3 col-form-label">Nama Lengkap <sup
                                style="color:red;">*</sup></label>
                        <div class="col-9">
                            <input type="text" readonly class="form-control" name="nama_lengkap" id="nama_lengkap"
                                placeholder="Password" value="">
                        </div>
                        <input type="hidden" name="id" id="id" value="">
                    </div>
                    <div class="form-group row">
                        <label for="password" class="col-3 col-form-label">No Telepon <sup
                                style="color:red;">*</sup></label>
                        <div class="col-9">
                            <input type="text" readonly class="form-control" name="no_telepon" id="no_telepon"
                                placeholder="Password" value="">
                        </div>
                        <input type="hidden" name="id" id="id" value="">
                    </div>
                    <div class="form-group row">
                        <label for="password" class="col-3 col-form-label">Email <sup style="color:red;">*</sup></label>
                        <div class="col-9">
                            <input type="text" readonly class="form-control" name="email" id="email"
                                placeholder="Password" value="">
                        </div>
                        <input type="hidden" name="id" id="id" value="">
                    </div>
                    <div class="form-group row">
                        <label for="password" class="col-3 col-form-label">Username <sup
                                style="color:red;">*</sup></label>
                        <div class="col-9">
                            <input type="text" readonly class="form-control" name="username" id="username"
                                placeholder="Password" value="">
                        </div>
                        <input type="hidden" name="id" id="id" value="">
                    </div>
                    <div class="form-group row">
                        <label for="password" class="col-3 col-form-label">Role <sup style="color:red;">*</sup></label>
                        <div class="col-9">
                            <input type="text" readonly class="form-control" name="role" id="role"
                                placeholder="Password" value="">
                        </div>
                        <input type="hidden" name="id" id="id" value="">
                    </div>
                    <div class="form-group row">
                        <label for="password" class="col-3 col-form-label">Password <sup
                                style="color:red;">*</sup></label>
                        <div class="col-9">
                            <input type="password" class="form-control" name="password" id="password"
                                placeholder="Password" value="" required>
                            <?php echo form_error('password') ?>
                        </div>
                        <input type="hidden" name="id" id="idx" value="">
                    </div>
                    <div class="form-group mb-0 justify-content-end row">
                        <div class="col-9">
                            <button type="submit" class="btn btn-success waves-effect waves-light"><i
                                    class="fa fa-save"></i> Submit</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>