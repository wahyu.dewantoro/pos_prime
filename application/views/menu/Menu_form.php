<div class="row">
            <div class="col-12">
                <div class="card-box">
                    <form action="<?php echo $action; ?>" method="post"  class="form-horizontal" role="form">
	    
                        <div class="form-group row">
                            <label for="nama_menu" class="col-3 col-form-label">Nama Menu <sup style="color:red;">*</sup></label>
                            <div class="col-9">
                                <input type="text" class="form-control"  name="nama_menu" id="nama_menu" placeholder="Nama Menu" value="<?php echo $nama_menu; ?>">
                                <?php echo form_error('nama_menu') ?>                                
                            </div>
                        </div>
	    
                        <div class="form-group row">
                            <label for="link_menu" class="col-3 col-form-label">Link Menu <sup style="color:red;">*</sup></label>
                            <div class="col-9">
                                <input type="text" class="form-control"  name="link_menu" id="link_menu" placeholder="Link Menu" value="<?php echo $link_menu; ?>">
                                <?php echo form_error('link_menu') ?>                                
                            </div>
                        </div>
	    
                        <div class="form-group row">
                            <label for="parent" class="col-3 col-form-label">Parent <sup style="color:red;">*</sup></label>
                            <div class="col-9">
                                
                                <select class="form-control"  name="parent" id="parent">
                                    <option value="0">Is parent</option>
                                    <?php foreach($parent_menu as $pm){?>
                                        <option <?php if($pm->id_inc==$parent){echo "selected";} ?> value="<?= $pm->id_inc?>"><?= $pm->nama_menu?></option>
                                    <?php } ?>
                                </select>
                                <?php echo form_error('parent') ?>                                
                            </div>
                        </div>
	    
                        <div class="form-group row">
                            <label for="sort" class="col-3 col-form-label">Sort <sup style="color:red;">*</sup></label>
                            <div class="col-9">
                                <input type="text" class="form-control"  name="sort" id="sort" placeholder="Sort" value="<?php echo $sort; ?>">
                                <?php echo form_error('sort') ?>                                
                            </div>
                        </div>
	    
                        <div class="form-group row">
                            <label for="icon" class="col-3 col-form-label">Icon <sup style="color:red;">*</sup></label>
                            <div class="col-9">
                                <input type="text" class="form-control"  name="icon" id="icon" placeholder="Icon" value="<?php echo $icon; ?>">
                                <?php echo form_error('icon') ?>                                
                            </div>
                        </div>
                        <input type="hidden" name="id_inc" value="<?= $id_inc ?>">
	                   <div class="form-group mb-0 justify-content-end row">
                            <div class="col-9">
                                <button type="submit" class="btn btn-success waves-effect waves-light"><i class="fa fa-save"></i> Submit</button>
                                <button type="reset" class="btn btn-warning waves-effect waves-light"><i class="fa fa-refresh"></i> Reset</button>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>