<div class="row">
            <div class="col-12">
                <div class="card-box">
                    <form action="<?php echo $action; ?>" method="post"  class="form-horizontal" role="form">
	    
                        <div class="form-group row">
                            <label for="nama_produk" class="col-3 col-form-label">Nama Produk <sup style="color:red;">*</sup></label>
                            <div class="col-9">
                                <input type="text" class="form-control"  name="nama_produk" id="nama_produk" placeholder="Nama Produk" value="<?php echo $nama_produk; ?>">
                                <?php echo form_error('nama_produk') ?>                                
                            </div>
                        </div>
	    
                        <div class="form-group row">
                            <label for="harga" class="col-3 col-form-label">Harga <sup style="color:red;">*</sup></label>
                            <div class="col-9">
                                <input type="text" class="form-control"  onkeyup="formatangka(this)" name="harga" id="harga" placeholder="Harga" value="<?php echo $harga; ?>">
                                <?php echo form_error('harga') ?>                                
                            </div>
                        </div>
	    
                        <div class="form-group row">
                            <label for="satuan" class="col-3 col-form-label">Satuan <sup style="color:red;">*</sup></label>
                            <div class="col-9">
                                <input type="text" class="form-control"  name="satuan" id="satuan" placeholder="Satuan" value="<?php echo $satuan; ?>">
                                <?php echo form_error('satuan') ?>                                
                            </div>
                        </div>
                        <input type="hidden" name="id" value="<?php echo $id; ?>" /> 
                        <div class="form-group mb-0 justify-content-end row">
                            <div class="col-9">
                                <button type="submit" class="btn btn-success waves-effect waves-light"><i class="fa fa-save"></i> Submit</button>
                                <button type="reset" class="btn btn-warning waves-effect waves-light"><i class="fa fa-refresh"></i> Reset</button>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>