<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Session Helper
 *
 * A simple session class helper for Codeigniter
 *
 * @package     Codeigniter Session Helper
 * @author      Dwayne Charrington
 * @copyright   Copyright (c) 2014, Dwayne Charrington
 * @license     http://www.apache.org/licenses/LICENSE-2.0.html
 * @link        http://ilikekillnerds.com
 * @since       Version 1.0
 * @filesource
 */

function safe($str)
{
    return strip_tags(trim($str));
}

function createFile($string, $path)
{
    $create = fopen($path, "w") or die("Change your permision folder for application and harviacode folder to 777");
    fwrite($create, $string);
    fclose($create);
    
    return $path;
}

function label($str)
{
    $label = str_replace('_', ' ', $str);
    $label = ucwords($label);
    return $label;
}


function angka($str)
{
    // $label = str_replace('_', ' ', $str);
    $label = number_format($str,'0','','.');
    return $label;
}


if (!function_exists('showMenu'))
{
    function showMenu($kode_group)
    {
        $CI = get_instance();
         $CI->menu_db=$CI->load->database('default',true); 
        $qmenu0  =$CI->menu_db->query("SELECT a.*
                                          FROM ms_menu a
                                          JOIN ms_privilege b ON a.id_inc=b.ms_menu_id
                                          WHERE ms_role_id in ($kode_group)
                                          AND STATUS='1'  
                                          AND parent='0' 
                                          ORDER BY sort ASC")->result_array();

        $varmenu='';
              foreach ($qmenu0 as $row0) {
                $parent  =$row0['id_inc'];
                $qmenu1  =$CI->menu_db->query("SELECT a.*
                                          FROM ms_menu a
                                          JOIN ms_privilege b ON a.id_inc=b.ms_menu_id
                                          WHERE ms_role_id in ($kode_group)
                                          AND STATUS='1'  
                                          AND parent='$parent' 
                                          ORDER BY sort ASC");
                $cekmenu =$qmenu1->num_rows();
                $dmenu1  =$qmenu1->result_array();
                  if($cekmenu>0){
                    $varmenu.= "<li class='has-submenu'>
                      <a href='#' > <i class='".$row0['icon']."'></i> ".ucwords($row0['nama_menu'])."</a>";
                        $varmenu.= "<ul class='submenu'>";
                          foreach($dmenu1 as $row1){
                              $varmenu.= "<li>".anchor(strtolower($row1['link_menu']),ucwords($row1['nama_menu']))."</li>";
                          }
                      $varmenu.= "</ul>
                      </li>";
                      }else{
                       $varmenu.= "<li class='has-submenu'>".anchor(strtolower($row0['link_menu']),"<i class='".$row0['icon']."'></i> ".ucwords($row0['nama_menu']))."</li>";
                    }
                  }
           echo $varmenu;
    }
}


if (!function_exists('errorbos'))
{
    function errorbos()
    {
            $CI = &get_instance();
            echo $CI->load->view('errors/403','',true);
            die();
    }
}

if (!function_exists('cek'))
{
    function cek($pengguna,$url,$var)
    {
        $CI = get_instance();
        $CI->menu_db=$CI->load->database('default',true); 

        if(empty($var)){
            errorbos();
        }
        switch ($var) {
            case 'read':
                # code...
                $vv='status';
                break;  
            case 'create':
                  # code...
            $vv='is_create';
                  break;  
            case 'update':
            $vv='is_update';
                # code...
                break;  
              case 'delete':
              $vv='is_delete';
                # code...
                break;
            default:
                # code...
                $vv='';
                break;
        }

        $res=$CI->menu_db->query("SELECT a.*
                                FROM ms_privilege a
                                JOIN ms_menu b ON a.ms_menu_id=b.id_inc
                                JOIN ms_assign_role d on d.ms_role_id=a.ms_role_id
                                JOIN ms_pengguna c ON c.id_inc=d.ms_pengguna_id
                                WHERE c.id_inc=$pengguna AND REPLACE(link_menu,'/','.')='$url' AND a.".$vv." ='1'")->row();

        if($res){
                return array(
                    'is_read'=>$res->status,
                    'is_create'=>$res->is_create,
                    'is_update'=>$res->is_update,
                    'is_delete'=>$res->is_delete
                );

        }else{
            errorbos();
        }
    }
}
 function get_client_ip() {
    $ipaddress = '';
    if (isset($_SERVER['HTTP_CLIENT_IP']))
        $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
    else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
        $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
    else if(isset($_SERVER['HTTP_X_FORWARDED']))
        $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
    else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
        $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
    else if(isset($_SERVER['HTTP_FORWARDED']))
        $ipaddress = $_SERVER['HTTP_FORWARDED'];
    else if(isset($_SERVER['REMOTE_ADDR']))
        $ipaddress = $_SERVER['REMOTE_ADDR'];
    else
        $ipaddress = 'UNKNOWN';
    return $ipaddress;
}