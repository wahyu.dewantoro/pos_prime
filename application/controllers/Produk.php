<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Produk extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Produk_model');
        $this->load->library('form_validation');
        $this->id_pengguna=get_userdata('app_id_pengguna');
    }

    private function cekAkses($var=null){
        $url='Produk';
        return cek($this->id_pengguna,$url,$var);
    }

    public function index()

    {
        $akses =$this->cekAkses('read');
        $q     = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));
        
        if ($q <> '') {
            $config['base_url']  = base_url() . 'produk?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'produk?q=' . urlencode($q);
        } else {
            $config['base_url']  = base_url() . 'produk';
            $config['first_url'] = base_url() . 'produk';
        }

        $config['per_page']          = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows']        = $this->Produk_model->total_rows($q);
        $produk                      = $this->Produk_model->get_limit_data($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'produk_data' => $produk,
            'q'                   => $q,
            'pagination'          => $this->pagination->create_links(),
            'total_rows'          => $config['total_rows'],
            'start'               => $start,
            'title'               => 'Data Produk',
            'create'              => 'Produk/create',
            'akses'               =>$akses
        );
        $this->template->load('layout','produk/Produk_list', $data);
    }

    

    public function create() 
    {
        $this->cekAkses('create');

        $data = array(
            'title'       =>'Tambah Data Produk',
            'kembali'     =>'Produk',
            'action'      => site_url('produk/create_action'),
            'id'          => set_value('id'),
            'nama_produk' => set_value('nama_produk'),
            'harga'       => set_value('harga'),
            'satuan'      => set_value('satuan'),
	);
        $this->template->load('layout','produk/Produk_form', $data);
    }
    
    public function create_action() 
    {
        $this->cekAkses('create');

        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
        'nama_produk' => $this->input->post('nama_produk',TRUE),
        'harga'       => str_replace('.','',$this->input->post('harga',TRUE)),
        'satuan'      => $this->input->post('satuan',TRUE),
	    );

            $this->Produk_model->insert($data);
            set_flashdata('success', 'Data telah di simpan.');
            redirect(site_url('produk'));
        }
    }
    
    public function update($ide) 
    {
        $this->cekAkses('update');
        $id=rapikan($ide);
        $row = $this->Produk_model->get_by_id($id);

        if ($row) {
            $data = array(
                'title'       => 'Edit data Produk',
                'action'      => site_url('produk/update_action'),
                'kembali'     =>'Produk',
                'id'          => set_value('id', $row->id),
                'nama_produk' => set_value('nama_produk', $row->nama_produk),
                'harga'       => set_value('harga', ribuan($row->harga)),
                'satuan'      => set_value('satuan', $row->satuan),
	    );
            $this->template->load('layout','produk/Produk_form', $data);
        } else {
            set_flashdata('warning', 'Record Not Found.');
            redirect(site_url('produk'));
        }
    }
    
    public function update_action() 
    {
        $this->cekAkses('update');
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id', TRUE));
        } else {
            $data = array(
        'nama_produk' => $this->input->post('nama_produk',TRUE),
        'harga'       => str_replace('.','',$this->input->post('harga',TRUE)),
        'satuan'      => $this->input->post('satuan',TRUE),
	    );

            $this->Produk_model->update($this->input->post('id', TRUE), $data);
            set_flashdata('success', 'Update Record Success');
            redirect(site_url('produk'));
        }
    }
    
    public function delete($ide) 
    {
        $this->cekAkses('delete');
        $id=rapikan($ide);
        $row = $this->Produk_model->get_by_id($id);

        if ($row) {
            $this->Produk_model->delete($id);
            set_flashdata('success', 'Delete Record Success');
            redirect(site_url('produk'));
        } else {
            set_flashdata('message', 'Record Not Found');
            redirect(site_url('produk'));
        }
    }

    public function _rules() 
    {
	$this->form_validation->set_rules('nama_produk', 'nama produk', 'trim|required');
	$this->form_validation->set_rules('harga', 'harga', 'trim|required');
	$this->form_validation->set_rules('satuan', 'satuan', 'trim|required');

	$this->form_validation->set_rules('id', 'id', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file Produk.php */
/* Location: ./application/controllers/Produk.php */