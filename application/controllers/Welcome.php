<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

    function __construct()
    {
        parent::__construct();
        $this->load->model('Penjualan_model');
        $this->id_pengguna=get_userdata('app_id_pengguna');
	}

    private function cekAkses($var=null){
        $url='Welcome';
        return cek($this->id_pengguna,$url,$var);
    }
	public function index()
	{
		$bulan=date('m');
		$tahun=date('Y');
		$harian=$this->db->query("SELECT DATE_FORMAT(tgl,'%d') tgl,IFNULL(jumlah,0) jumlah from (select * from
		(select adddate('1970-01-01',t4.i*10000 + t3.i*1000 + t2.i*100 + t1.i*10 + t0.i) tgl from
		 (select 0 i union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t0,
		 (select 0 i union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t1,
		 (select 0 i union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t2,
		 (select 0 i union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t3,
		 (select 0 i union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t4) v
		WHERE month(tgl) = '$bulan' and year(tgl) = '$tahun')a left JOIN (
		SELECT date(tanggal_transaksi) as bulan , sum(jumlahtransaksi(id)) jumlah FROM pos_penjualan WHERE month(tanggal_transaksi) = '$bulan' and year(tanggal_transaksi) = '$tahun' GROUP BY date(tanggal_transaksi))b on a.tgl=b.bulan")->result();
		$bulanan=$this->db->query("SELECT a.BULAN,IFNULL(jumlah,0) jumlah from (
			SELECT
				1 AS BULAN UNION
			SELECT
				2 AS BULAN UNION
			SELECT
				3 AS BULAN UNION
			SELECT
				4 AS BULAN UNION
			SELECT
				5 AS BULAN UNION
			SELECT
				6 AS BULAN UNION
			SELECT
				7 AS BULAN UNION
			SELECT
				8 AS BULAN UNION
			SELECT
				9 AS BULAN UNION
			SELECT
				10 AS BULAN UNION
			SELECT
				11 AS BULAN UNION
			SELECT
				12 AS BULAN
			)a left join (

			SELECT month(tanggal_transaksi) as bulan , sum(jumlahtransaksi(id)) jumlah FROM pos_penjualan WHERE year(tanggal_transaksi) = '$tahun' GROUP BY month(tanggal_transaksi)) b on a.BULAN=b.bulan")->result();

			$akses =$this->cekAkses('read');
			$config['per_page']          = 15;
			$start = intval(0);
			$penjualan                      = $this->Penjualan_model->get_limit_data($config['per_page'], $start,null);
		$data=array(

			'title'=>'Dashboard',
			'script'         =>'js_dashboard',
			'harian'		=>$harian,
			'bulanan'		=>$bulanan,
            'penjualan_data' => $penjualan,
            'start' => $start,
            'akses'          =>$akses,
		);
		$this->template->load('layout','dashboard',$data);
		// echo base_url();
	}
}
