<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

require_once APPPATH.'/third_party/spout/src/Spout/Autoloader/autoload.php';
use Box\Spout\Writer\WriterFactory;
use Box\Spout\Common\Type;
use Box\Spout\Writer\Style\StyleBuilder;
class Lap_rekap_pembelian extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Pembelian_model');
        $this->load->model('Supplier_model');
        $this->load->model('Produk_model');
        $this->load->library('form_validation');
        $this->id_pengguna=get_userdata('app_id_pengguna');
        $this->username=get_userdata('app_username');
    }
    private function cekAkses($var=null){
        $url='lap_rekap_pembelian';
        return cek($this->id_pengguna,$url,$var);
    }

    public function index()

    {
        $akses =$this->cekAkses('read');
        $date1=urldecode($this->input->get('date1',true));
        $date2=urldecode($this->input->get('date2',true));
        $start = intval($this->input->get('start'));

        if($date1==''){
            $date1=date('Y-m-d');
        }

        if($date2==''){
            $date2=date('Y-m-d');
        }

        if ($date1 <> '' || $date2 <> '') {
            $config['base_url']  = base_url() . 'lap_rekap_pembelian?date1='.urlencode($date1).'&date2='.urlencode($date2);
            $config['first_url'] = base_url() . 'lap_rekap_pembelian?date1='.urlencode($date1).'&date2='.urlencode($date2);
            $cetak = base_url() . 'lap_rekap_pembelian/cetak?date1='.urlencode($date1).'&date2='.urlencode($date2);
        } else {
            $config['base_url']  = base_url() . 'lap_rekap_pembelian';
            $config['first_url'] = base_url() . 'lap_rekap_pembelian';
            $cetak = base_url() . 'lap_rekap_pembelian';
        }

        $config['per_page']          = 10;
        $config['page_query_string'] = TRUE;
        $this->db->where("date(tgl_pembelian) BETWEEN '".$date1."' AND '".$date2."'");
        $config['total_rows']        = $this->Pembelian_model->laporan_total_rows();
        $this->db->where("date(tgl_pembelian) BETWEEN '".$date1."' AND '".$date2."'");
        $pembelian                        = $this->Pembelian_model->laporan_get_limit_data($config['per_page'], $start);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'pembelian_data'      => $pembelian,
            'pagination'          => $this->pagination->create_links(),
            'total_rows'          => $config['total_rows'],
            'start'               => $start,
            'date1'               => $date1,
            'date2'               => $date2,
            'cetak'               => $cetak,
            'title'               => 'Laporan Rekap Pembelian',
            'akses'               => $akses
        );
        $this->template->load('layout','lap_rekap_pembelian/lap_rekap_pembelian_list', $data);
    }
    function cetak()
    {
        ini_set('memory_limit', '-1');
        $date1=urldecode($this->input->get('date1',true));
        $date2=urldecode($this->input->get('date2',true));

        if($date1==''){
            $date1=date('Y-m-d');
        }

        if($date2==''){
            $date2=date('Y-m-d');
        }
        $this->db->where("date(tgl_pembelian) BETWEEN '".$date1."' AND '".$date2."'");
        $pembelian                        = $this->Pembelian_model->getAll_data();
        $header=['NO', 'Tanggal Pembelian', 'Nama Supplier', 'Nama Petugas', 'Kode Barang', 'Nama Barang','Jumlah', 'Harga', 'TOtal Pembelian'];
        // setup Spout Excel Writer, set tipenya xlsx
        $writer = WriterFactory::create(Type::XLSX);
        // download to browser

        // set style untuk header
        $headerStyle = (new StyleBuilder())
               ->setFontBold()
               ->build();
    $writer->setTempFolder('tmp/'); //define temporary folder yg akan digunakan untuk menampung hasil file yg ditulis sementara
        $namaFile = 'Laporan_Rekap_Pembelian_'.date('YmdHis').'.xlsx'; //nama filenya
        $filePath = 'tmp/' . $namaFile;

     $defaultStyle = (new StyleBuilder())
                ->setFontName('Arial')
                ->setFontSize(10)
                ->setShouldWrapText(false)
                ->build();
        $writer->setDefaultRowStyle($defaultStyle)
                ->openToFile($filePath);
             /*echo $this->db->last_query();
             echo "<hr>";*/
             $arrisi =array();
    $no=1;
    foreach($pembelian as $rk){
        $ff=array($no,date_indo($rk->tgl_pembelian),$rk->nama_suplier,$rk->nama_pegawai,$rk->kd_barang,$rk->nama_barang,$rk->qty_pembelian,$rk->harga_per_item,$rk->harga_jumlah);
        array_push($arrisi,$ff);
        $no++;
    }
    // write ke Sheet kedua
    $writer->getCurrentSheet()->setName('Pembelian');
    // header Sheet kedua
    $writer->addRowWithStyle($header, $headerStyle);
    // data Sheet pertama
    $writer->addRows($arrisi);


     // close writter
        $writer->close();
        $this->load->helper('download');
        force_download($filePath, null);
    }
}