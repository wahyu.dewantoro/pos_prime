<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Supplier extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Supplier_model');
        $this->load->library('form_validation');
        $this->id_pengguna=get_userdata('app_id_pengguna');
    }



    private function cekAkses($var=null){
        $url='Supplier';
        return cek($this->id_pengguna,$url,$var);
    }

    public function index()

    {
        $akses =$this->cekAkses('read');
        $q     = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));

        if ($q <> '') {
            $config['base_url']  = base_url() . 'supplier?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'supplier?q=' . urlencode($q);
        } else {
            $config['base_url']  = base_url() . 'supplier';
            $config['first_url'] = base_url() . 'supplier';
        }

        $config['per_page']          = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows']        = $this->Supplier_model->total_rows($q);
        $supplier                        = $this->Supplier_model->get_limit_data($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'supplier_data' => $supplier,
            'q'                   => $q,
            'pagination'          => $this->pagination->create_links(),
            'total_rows'          => $config['total_rows'],
            'start'               => $start,
            'title'               => 'Data Supplier',
            'create'              => 'Supplier/create',
            'akses'               =>$akses
        );
        $this->template->load('layout','supplier/Supplier_list', $data);
    }



    public function create()
    {
        $this->cekAkses('create');
        $data = array(
            'title'     =>'Tambah Data Supplier',
            'kembali'   =>'supplier',
            'action'    => site_url('supplier/create_action'),
            'id'        => set_value('id'),
            'nama'      => set_value('nama'),
            'hp'        => set_value('hp'),
            'alamat'    => set_value('alamat'),
	);
        $this->template->load('layout','supplier/Supplier_form', $data);
    }

    public function create_action()
    {
        $this->cekAkses('create');

        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
                    'nama'      => $this->input->post('nama',TRUE),
                    'hp'        => $this->input->post('hp',TRUE),
                    'alamat'    => $this->input->post('alamat',TRUE),
            	    );

            $this->Supplier_model->insert($data);
           set_flashdata('success', 'Data telah di simpan.');
            redirect(site_url('supplier'));
        }
    }

    public function update($ide)
    {
        $this->cekAkses('update');
        $id=rapikan($ide);
        $row = $this->Supplier_model->get_by_id($id);

        if ($row) {
            $data = array(
                'title'     => 'Edit Data Supplier',
                'action'    => site_url('supplier/update_action'),
                'kembali'   =>'supplier',
                'id'        => set_value('id', $row->id),
                'nama'      => set_value('nama', $row->nama),
                'hp'        => set_value('hp', $row->hp),
                'alamat'    => set_value('alamat', $row->alamat),
	    );
            $this->template->load('layout','supplier/Supplier_form', $data);
        } else {
           set_flashdata('warning', 'Record Not Found.');
            redirect(site_url('supplier'));
        }
    }

    public function update_action()
    {
        $this->cekAkses('update');
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id_inc', TRUE));
        } else {
            $data = array(
                'nama'      => $this->input->post('nama',TRUE),
                'hp'        => $this->input->post('hp',TRUE),
                'alamat'    => $this->input->post('alamat',TRUE),
	    );
            $this->Supplier_model->update($this->input->post('id', TRUE), $data);

            // echo $this->db->last_query();

           set_flashdata('success', 'Update Record Success');
            redirect(site_url('supplier'));
        }
    }

    public function delete($ide)
    {
        $this->cekAkses('delete');
        $id=rapikan($ide);
        $row = $this->Supplier_model->get_by_id($id);

        if ($row) {
            $this->Supplier_model->delete($id);
           set_flashdata('success', 'Delete Record Success');
            redirect(site_url('supplier'));
        } else {
           set_flashdata('message', 'Record Not Found');
            redirect(site_url('supplier'));
        }
    }

    public function _rules()
    {
	$this->form_validation->set_rules('nama', 'nama', 'trim|required');
	$this->form_validation->set_rules('hp', 'link', 'trim|required|numeric');
	$this->form_validation->set_rules('alamat', 'alamat', 'trim|required');

	$this->form_validation->set_rules('id', 'id', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file Menu.php */
/* Location: ./application/controllers/Menu.php */