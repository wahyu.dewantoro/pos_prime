<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Role extends CI_Controller {

	function __construct()
    {
        parent::__construct();
        $this->load->model('Msistem');
        $this->id_pengguna=get_userdata('app_id_pengguna');
    }

    private function cekAkses($var=null){
        $url='role';
        return cek($this->id_pengguna,$url,$var);
    }

	public function index()
	{

		$akses =$this->cekAkses('read');

		$data=array(
			'title' => 'Role',
			'data'  => $this->Msistem->getRole()->result(),
			'akses' => $akses,
            'create'=>'role/add'
            // 'script'=> 'sistem/jsrole'
		);
		$this->template->load('layout','sistem/role_list',$data);
	}

	function add(){
		$akses =$this->cekAkses('create');
		$data=array(
			'title'     =>'Tambah Role',
			'nama_role' => null,
			'id_inc'    =>null,
			'action'    =>base_url().'role/add_action'
		);
		$this->template->load('layout','sistem/role_form',$data);
	}

    function edit(){
        $akses =$this->cekAkses('update'); 
        $id=rapikan($this->input->get('q',true));
        $this->db->where('id_inc',$id);
        $res=$this->db->get('ms_role')->row();
        $data=array(
            'title'     =>'Edit Role',
            'nama_role' => $res->nama_role,
            'id_inc'    =>$res->id_inc,
            'action'    =>base_url().'role/edit_action'
        );
        $this->template->load('layout','sistem/role_form',$data);
    }

    function edit_action(){
        $this->cekAkses('update');
        $this->db->where('id_inc',$this->input->post('id_inc',true));
        $this->db->set('nama_role',$this->input->post('nama_role',true));
        $res=$this->db->update('ms_role');
        if($res){
            set_flashdata('success','Berhasil, Data telah di simpan');
        }else{
            set_flashdata('warning','Gagal, Data tidak di simpan');
        }

        redirect(site_url('role'));
    }

    function add_action(){
        $akses =$this->cekAkses('create'); 
        $this->db->set('nama_role',$this->input->post('nama_role',true));
        $res=$this->db->insert('ms_role');
        if($res){
            set_flashdata('success','Berhasil, Data telah di simpan');
        }else{
            set_flashdata('warning','Gagal, Data tidak di simpan');
        }

        redirect(site_url('role'));
    }


    function delete(){
        $akses =$this->cekAkses('delete'); 
        $ide=$this->input->get('q');
        $id=rapikan($ide);
        $this->db->where('id_inc',$id);
        $res=$this->db->delete('ms_role');
        if($res){
            set_flashdata('success','Berhasil, Data telah di hapus');
        }else{
            set_flashdata('warning','Gagal, Data tidak di hapus');
        }

        redirect(site_url('role'));
    }

	function setting(){
		$akses =$this->cekAkses('update'); 
		$ide =$this->input->get('q');
		$id  =rapikan($ide);
		$this->db->where('id_inc',$id);
		$row = $this->Msistem->getRole()->row();
        if ($row) {
            $role=$this->Msistem->getMenu($id);
            $data = array(
				'id_inc' => $row->id_inc,
				'role'   =>$role,
				'title'  =>'Setting Privilege : '.$row->nama_role,
				'akses'  =>null
                );
            $this->template->load('layout','sistem/view_setting_read',$data);
        } else {
            // $this->session->set_flashdata('message', 'Record Not Found');
            set_flashdata('warning','Data tidak tersedia.');
            redirect(site_url('role'));
        }
	}

	function prosessettingrole(){
		$akses =$this->cekAkses('update'); 
		 $kode_group =$_POST['kode_group'];
        $roles      =$_POST['role'];
        

        if(isset($_POST['create'])){
            $create =$_POST['create'];
            // create
            $idc='';
            while ($aa = current($create)) {
                if ($aa== '1') {
                   $idc.=key($create).',';
                }
            next($create);
            }
            $idc=substr($idc, 0,-1);
        }else{
            // $create=array();
            $idc=null;
        }
        
    
        if(isset($_POST['update'])){
            $update =$_POST['update'];
            // update
            $idu='';
            while ($ab = current($update)) {
                if ($ab== '1') {
                   $idu.=key($update).',';
                }
                next($update);
            }
            $idu=substr($idu, 0,-1);
        }else{
            $idu=null;
            // $update=array();
        }
        
        if(isset($_POST['delete'])){
            $delete     =$_POST['delete'];    
            // delete
            $idd='';
            while ($ac = current($delete)) {
                if ($ac== '1') {
                   $idd.=key($delete).',';
                }
            next($delete);
            }
            $idd=substr($idd, 0,-1);
        }else{
            // $delete=array();
            $idd=null;
        }
        
        $data['kode_role']=$kode_group;
        $data['read']=implode(',',$roles);
        $data['create']=$idc;
        $data['update']=$idu;
        $data['delete']=$idd;
        
       /* echo "<pre>";
        print_r($data);
        echo "</pre>";*/

        $role=$this->Msistem->prosesrole($data);

        // echo $role;
        if($role){
            set_flashdata('success','Berhasil, Data privilege telah di perbarui');
        }else{
            set_flashdata('warning','Berhasil, Data gagal di proses.');
        }
        redirect('role');
	}

}
