<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Printer extends CI_Controller {

	function __construct()
    {
        parent::__construct();
        $this->load->model('Msistem');
        $this->id_pengguna=get_userdata('app_id_pengguna');
    }

    private function cekAkses($var=null){
        $url='printer';
        return cek($this->id_pengguna,$url,$var);
    }

	public function index()
	{

		$akses =$this->cekAkses('read');

        $this->db->where('id_inc', $this->id_pengguna);
		$data=array(
			'title' => 'Printer',
			'data'  => $this->db->get('ms_pengguna')->result(),
			'akses' => $akses,
            'create'=>'role/add'
            // 'script'=> 'sistem/jsrole'
		);
		$this->template->load('layout','sistem/print_list',$data);
	}

    function edit(){
        $akses =$this->cekAkses('update');
        $id=rapikan($this->input->get('q',true));
        $this->db->where('id_inc',$id);
        $res=$this->db->get('ms_pengguna')->row();
        $data=array(
            'title'     =>'Edit Printer',
            'ip_printer' => $res->ip_printer,
            'id_inc'    =>$res->id_inc,
            'action'    =>base_url().'printer/edit_action'
        );
        $this->template->load('layout2','sistem/print_form',$data);
    }

    function edit_action(){
        $this->cekAkses('update');
        $this->db->where('id_inc',$this->input->post('id_inc',true));
        $this->db->set('ip_printer',$this->input->post('ip_printer',true));
        $res=$this->db->update('ms_pengguna');
        if($res){
            set_userdata('app_ip_printer',$this->input->post('ip_printer',true));
            set_flashdata('success','Berhasil, Data telah di simpan');
        }else{
            set_flashdata('warning','Gagal, Data tidak di simpan');
        }

        redirect(site_url('printer'));
    }




}
