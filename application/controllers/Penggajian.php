<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Penggajian extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Penggajian_model');
        $this->load->library('form_validation');
        $this->id_pengguna=get_userdata('app_id_pengguna');
    }

    private function cekAkses($var=null){
        $url='Penggajian';
        return cek($this->id_pengguna,$url,$var);
    }

    public function index()

    {
        $akses =$this->cekAkses('read');
        $q     = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));

        if ($q <> '') {
            $config['base_url']  = base_url() . 'penggajian?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'penggajian?q=' . urlencode($q);
        } else {
            $config['base_url']  = base_url() . 'penggajian';
            $config['first_url'] = base_url() . 'penggajian';
        }

        $config['per_page']          = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows']        = $this->Penggajian_model->total_rows($q);
        $penggajian                      = $this->Penggajian_model->get_limit_data($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'penggajian_data' => $penggajian,
            'q'                   => $q,
            'pagination'          => $this->pagination->create_links(),
            'total_rows'          => $config['total_rows'],
            'start'               => $start,
            'title'               => 'Data Penggajian',
            'create'              => 'Penggajian/create',
            'akses'               =>$akses
        );
        $this->template->load('layout','penggajian/Penggajian_list', $data);
    }



    public function create()
    {
        $this->cekAkses('create');



        $data = array(
            'title'   =>'Tambah Data Penggajian',
            'kembali' =>'Penggajian',
            'action'  => site_url('penggajian/create_action'),
	    'id' => set_value('id'),
	    'tanggal_transaksi' => set_value('tanggal_transaksi', date('Y-m-d')),
	    'bulan_gaji' => set_value('bulan_gaji'),
	    'tahun_gaji' => set_value('tahun_gaji'),
	    'nip_pegawai' => set_value('nip_pegawai'),
	    'nama_pegawai' => set_value('nama_pegawai'),
	    'gaji_pokok' => set_value('gaji_pokok'),
	    'tunjangan' => set_value('tunjangan'),
	    'bonus' => set_value('bonus'),
	    'date_insert' => set_value('date_insert'),
	    'nip_insert' => set_value('nip_insert'),
        'script'       =>'penggajian/js_gaji',
        'pegawai'      =>$this->Penggajian_model->getPegawai()
	);
        $this->template->load('layout','penggajian/Penggajian_form', $data);
    }

    public function create_action()
    {
        $this->cekAkses('create');

        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'tanggal_transaksi' => $this->input->post('tanggal_transaksi',TRUE),
		'bulan_gaji' => $this->input->post('bulan_gaji',TRUE),
		'tahun_gaji' => $this->input->post('tahun_gaji',TRUE),
		'nip_pegawai' => $this->input->post('nip_pegawai',TRUE),
		'nama_pegawai' => $this->input->post('nama_pegawai',TRUE),
		'gaji_pokok' => str_replace(".","",$this->input->post('gaji_pokok',TRUE)),
		'tunjangan' => str_replace(".","",$this->input->post('tunjangan',TRUE)),
		'bonus' => str_replace(".","",$this->input->post('bonus',TRUE)),
		'date_insert' => date('Y-m-d'),
		'nip_insert' => $this->id_pengguna,
	    );

            $this->Penggajian_model->insert($data);
            set_flashdata('success', 'Data telah di simpan.');
            redirect(site_url('penggajian'));
        }
    }

    public function update($ide)
    {
        $this->cekAkses('update');
        $id=rapikan($ide);
        $row = $this->Penggajian_model->get_by_id($id);

        if ($row) {
            $data = array(
                'title' => 'Edit data Penggajian',
                'action' => site_url('penggajian/update_action'),
                'kembali' =>'Penggajian',
		'id' => set_value('id', $row->id),
		'tanggal_transaksi' => set_value('tanggal_transaksi', $row->tanggal_transaksi),
		'bulan_gaji' => set_value('bulan_gaji', $row->bulan_gaji),
		'tahun_gaji' => set_value('tahun_gaji', $row->tahun_gaji),
		'nip_pegawai' => set_value('nip_pegawai', $row->nip_pegawai),
		'nama_pegawai' => set_value('nama_pegawai', $row->nama_pegawai),
		'gaji_pokok' => set_value('gaji_pokok', number_format($row->gaji_pokok,0,",",".")),
		'tunjangan' => set_value('tunjangan', number_format($row->tunjangan,0,",",".")),
		'bonus' => set_value('bonus', number_format($row->bonus,0,",",".")),
        'script'       =>'penggajian/js_gaji',
        'pegawai'      =>$this->Penggajian_model->getPegawai()
	    );
            $this->template->load('layout','penggajian/Penggajian_form', $data);
        } else {
            set_flashdata('warning', 'Record Not Found.');
            redirect(site_url('penggajian'));
        }
    }

    public function update_action()
    {
        $this->cekAkses('update');
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id', TRUE));
        } else {
            $data = array(
		'tanggal_transaksi' => $this->input->post('tanggal_transaksi',TRUE),
		'bulan_gaji' => $this->input->post('bulan_gaji',TRUE),
		'tahun_gaji' => $this->input->post('tahun_gaji',TRUE),
		'nip_pegawai' => $this->input->post('nip_pegawai',TRUE),
		'nama_pegawai' => $this->input->post('nama_pegawai',TRUE),
		'gaji_pokok' => str_replace(".","",$this->input->post('gaji_pokok',TRUE)),
		'tunjangan' => str_replace(".","",$this->input->post('tunjangan',TRUE)),
		'bonus' => str_replace(".","",$this->input->post('bonus',TRUE)),
	    );

            $this->Penggajian_model->update($this->input->post('id', TRUE), $data);
            set_flashdata('success', 'Update Record Success');
            redirect(site_url('penggajian'));
        }
    }

    public function delete($ide)
    {
        $this->cekAkses('delete');
        $id=rapikan($ide);
        $row = $this->Penggajian_model->get_by_id($id);

        if ($row) {
            $this->Penggajian_model->delete($id);
            set_flashdata('success', 'Delete Record Success');
            redirect(site_url('penggajian'));
        } else {
            set_flashdata('message', 'Record Not Found');
            redirect(site_url('penggajian'));
        }
    }

    public function _rules()
    {
	$this->form_validation->set_rules('tanggal_transaksi', 'tanggal transaksi', 'trim|required');
	$this->form_validation->set_rules('bulan_gaji', 'bulan gaji', 'trim|required');
	$this->form_validation->set_rules('tahun_gaji', 'tahun gaji', 'trim|required');
	$this->form_validation->set_rules('nip_pegawai', 'nip pegawai', 'trim|required');
	$this->form_validation->set_rules('nama_pegawai', 'nama pegawai', 'trim|required');

	$this->form_validation->set_rules('id', 'id', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file Penggajian.php */
/* Location: ./application/controllers/Penggajian.php */