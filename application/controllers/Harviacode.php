<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Harviacode extends CI_Controller {

	

	function __construct()
    {
        parent::__construct();
    	// $subject = file_get_contents('../application/config/database.php');
    	$this->namedb=$this->db->database;
    	$this->id_pengguna=get_userdata('app_id_pengguna');
    }

    private function cekAkses($var=null){
        $url='harviacode';
        return cek($this->id_pengguna,$url,$var);
    }

    private function list_table(){
    	
    	return $this->db->query("SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA='".$this->namedb."' AND table_type='BASE TABLE'")->result();
    }

    private function primary_field($table)
    {
    	$db= $this->namedb;
    	$query=$this->db->query("SELECT COLUMN_NAME,COLUMN_KEY FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA='$db' AND TABLE_NAME='$table' AND COLUMN_KEY = 'PRI'")->row();

    	if($query){
    		return $query->COLUMN_NAME;
    	}else{
    		return null;
    	}
    }

    private function not_primary_field($table)
    {
    	$db= $this->namedb;
    	$query=$this->db->query("SELECT COLUMN_NAME,COLUMN_KEY,DATA_TYPE FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA='$db' AND TABLE_NAME='$table' AND COLUMN_KEY <> 'PRI'")->result();

    	foreach($query as $qq){
    		$fields[] = array('column_name' => $qq->COLUMN_NAME, 'column_key' => $qq->COLUMN_KEY, 'data_type' => $qq->DATA_TYPE);
    	}

    	return $fields;
    }

    private function all_field($table)
    {
		$db= $this->namedb;
    	$query=$this->db->query("SELECT COLUMN_NAME,COLUMN_KEY,DATA_TYPE FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA='$db' AND TABLE_NAME='$table'")->result();

    	foreach($query as $rk){
    		$fields[] = array('column_name' => $rk->COLUMN_NAME, 'column_key' => $rk->COLUMN_KEY, 'data_type' => $rk->DATA_TYPE);
    	}

    	return $fields;
    }



	public function index()
	{
		$akses =$this->cekAkses('read');
		$data=array(
			'title'=>'Crud Generator By: Harviacode',
			'script'=>'Harviacode/jsindex',
			'list_table'=>$this->list_table()
		);
		$this->template->load('layout','Harviacode/index',$data);
	}

	function proses(){
		$akses =$this->cekAkses('create');
		
		$table_name   = safe($this->input->post('table_name',true));
		$controller   = safe($this->input->post('controller',true));
		$model        = safe($this->input->post('model',true));
		$jenis_tabel  = 'reguler_table';
		$export_excel = '';
		$export_word  = '';
		$export_pdf   = '';

		if ($table_name <> '')
	    {
	    	// set data
			$table_name = $table_name;
			$c          = $controller <> '' ? ucfirst($controller) : ucfirst($table_name);
			$m          = $model <> '' ? ucfirst($model) : ucfirst($table_name) . '_model';
			$v_list     = $c . "_list";
			$v_form     = $c . "_form";
			// $v_read     = $table_name . "_read";
			/*$v_doc      = $table_name . "_doc";
			$v_pdf      = $table_name . "_pdf";*/
		
			$c_url = strtolower($c);

			// filename
			$c_file      = $c . '.php';
			$m_file      = $m . '.php';
			$v_list_file = $v_list . '.php';
			$v_form_file = $v_form . '.php';
			/*$v_read_file = $v_read . '.php';
			$v_doc_file  = $v_doc . '.php';
			$v_pdf_file  = $v_pdf . '.php';*/

			$target ='application/';
			if (!file_exists($target . "views/" . $c_url))
	        {
	            mkdir($target . "views/" . $c_url, 0777, true);
	        }

			$pk     = $this->primary_field($table_name);
			$non_pk = $this->not_primary_field($table_name);
			$all    = $this->all_field($table_name);

			require  'core/create_config_pagination.php';
			require  'core/create_controller.php';
			require  'core/create_model.php';
			
			// if ($jenis_tabel == 'reguler_table') {
			require  'core/create_view_list.php';
			/*} else {
				require  'core/create_view_list_datatables.php';
				require  'core/create_libraries_datatables.php';
			}*/

			require  'core/create_view_form.php';
			// include 'core/create_view_read.php';
			$hasil[] = $hasil_controller;
			$hasil[] = $hasil_model;
			$hasil[] = $hasil_view_list;
			$hasil[] = $hasil_view_form;
			// $hasil[] = $hasil_view_read;
			// $hasil[] = $hasil_view_doc;
			// $hasil[] = $hasil_view_pdf;
			$hasil[] = $hasil_config_pagination;
			// $hasil[] = $hasil_exportexcel;
			// $hasil[] = $hasil_pdf;

			set_flashdata('success','Crud telah di buat!');
			$this->session->set_flashdata('hasil',$hasil);
		}else{
			set_flashdata('warning','Tidak ada table yang di pilih !');
			
		}
		redirect('harviacode');
	}
}
