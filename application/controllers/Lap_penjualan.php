<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

require_once APPPATH.'/third_party/spout/src/Spout/Autoloader/autoload.php';
use Box\Spout\Writer\WriterFactory;
use Box\Spout\Common\Type;
use Box\Spout\Writer\Style\StyleBuilder;

class Lap_Penjualan extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Penjualan_model');
        $this->load->model('Produk_model');
        $this->load->library('form_validation');
        $this->id_pengguna=get_userdata('app_id_pengguna');
        $this->username=get_userdata('app_username');
    }
    private function cekAkses($var=null){
        $url='Lap_penjualan';
        return cek($this->id_pengguna,$url,$var);
    }

    public function index()

    {
        $akses =$this->cekAkses('read');
        $date1=urldecode($this->input->get('date1',true));
        $date2=urldecode($this->input->get('date2',true));
        $start = intval($this->input->get('start'));

        if($date1==''){
            $date1=date('Y-m-d');
        }

        if($date2==''){
            $date2=date('Y-m-d');
        }

        if ($date1 <> '' || $date2 <> '') {
            $config['base_url']  = base_url() . 'lap_penjualan?date1='.urlencode($date1).'&date2='.urlencode($date2);
            $config['first_url'] = base_url() . 'lap_penjualan?date1='.urlencode($date1).'&date2='.urlencode($date2);
            $cetak = base_url() . 'lap_penjualan/cetak?date1='.urlencode($date1).'&date2='.urlencode($date2);
            $cetak2 = base_url() . 'lap_penjualan/cetak2?date1='.urlencode($date1).'&date2='.urlencode($date2);
        } else {
            $config['base_url']  = base_url() . 'lap_penjualan';
            $config['first_url'] = base_url() . 'lap_penjualan';
            $cetak = base_url() . 'lap_penjualan';
            $cetak2 = base_url() . 'lap_penjualan';
        }
        $config['per_page']          = 10;
        $config['page_query_string'] = TRUE;
        $this->db->where("date(tanggal_transaksi) BETWEEN '".$date1."' AND '".$date2."'");
        $config['total_rows']        = $this->Penjualan_model->laporan_total_rows();
        $this->db->where("date(tanggal_transaksi) BETWEEN '".$date1."' AND '".$date2."'");
        $Penjualan                        = $this->Penjualan_model->laporan_get_limit_data($config['per_page'], $start);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'penjualan_data'      => $Penjualan,
            'pagination'          => $this->pagination->create_links(),
            'total_rows'          => $config['total_rows'],
            'start'               => $start,
            'date1'               => $date1,
            'date2'               => $date2,
            'cetak'               => $cetak,
            'cetak2'              => $cetak2,
            'title'               => 'Laporan Penjualan',
            'akses'               => $akses
        );
        $this->template->load('layout','lap_Penjualan/lap_Penjualan_list', $data);
    }

    function cetak()
    {
        ini_set('memory_limit', '-1');
        $date1=urldecode($this->input->get('date1',true));
        $date2=urldecode($this->input->get('date2',true));

        if($date1==''){
            $date1=date('Y-m-d');
        }

        if($date2==''){
            $date2=date('Y-m-d');
        }
        $this->db->where("date(tanggal_transaksi) BETWEEN '".$date1."' AND '".$date2."'");
        $Penjualan                        = $this->Penjualan_model->getAll_data();
        $header=['NO', 'Kode Transaksi', 'Kasir', 'Pelanggan', 'Tanggal Transaksi', 'Jumlah'];
        // setup Spout Excel Writer, set tipenya xlsx
        $writer = WriterFactory::create(Type::XLSX);
        // download to browser

        // set style untuk header
        $headerStyle = (new StyleBuilder())
               ->setFontBold()
               ->build();



    $writer->setTempFolder('tmp/'); //define temporary folder yg akan digunakan untuk menampung hasil file yg ditulis sementara
        $namaFile = 'Laporan_Penjualan_'.date('YmdHis').'.xlsx'; //nama filenya
        $filePath = 'tmp/' . $namaFile;

     $defaultStyle = (new StyleBuilder())
                ->setFontName('Arial')
                ->setFontSize(10)
                ->setShouldWrapText(false)
                ->build();
        $writer->setDefaultRowStyle($defaultStyle)
                ->openToFile($filePath);
             /*echo $this->db->last_query();
             echo "<hr>";*/
             $arrisi =array();
    $no=1;
    foreach($Penjualan as $rk){
        $ff=array($no,$rk->kode_transaksi,$rk->nama_pegawai,$rk->nama_pelanggan,$rk->tanggal_transaksi,$rk->jumlah);
        array_push($arrisi,$ff);
        $no++;
    }
    // write ke Sheet kedua
    $writer->getCurrentSheet()->setName('Penjualan');
    // header Sheet kedua
    $writer->addRowWithStyle($header, $headerStyle);
    // data Sheet pertama
    $writer->addRows($arrisi);


     // close writter
        $writer->close();
        $this->load->helper('download');
        force_download($filePath, null);
    }
    function cetak2()
    {
        ini_set('memory_limit', '-1');
        $date1=urldecode($this->input->get('date1',true));
        $date2=urldecode($this->input->get('date2',true));

        if($date1==''){
            $date1=date('Y-m-d');
        }

        if($date2==''){
            $date2=date('Y-m-d');
        }
        $this->db->where("date(tanggal_transaksi) BETWEEN '".$date1."' AND '".$date2."'");
        $Penjualan                        = $this->Penjualan_model->getAll_data();
        $id = array();
        foreach ($Penjualan as $rk) {
            $id[]=$rk->id;
        }
        // $detail=$this->db->query("SELECT b.kode_transaksi,b.tanggal_transaksi,a.* FROM pos_penjualan_item a join pos_penjualan b on a.pos_penjualan_id=b.id WHERE  in (".$id.")")->result();
        $this->db->select('b.kode_transaksi,b.tanggal_transaksi,a.*');
        $this->db->from('pos_penjualan_item a');
        $this->db->join('pos_penjualan b', 'a.pos_penjualan_id = b.id');
        $this->db->where_in('pos_penjualan_id', $id);
        // or $this->db->or_where_in();
        $query = $this->db->get();
        $sql  = $query->result();
        // echo "<pre>";
        // var_dump($sql);
        // echo "</pre>";
        // echo $this->db->last_query();
        // exit();
        $header=['NO', 'Kode Transaksi', 'Tanggal Transaksi', 'Nama Produk', 'Satuan','Qty','harga' ,'Jumlah'];
        // setup Spout Excel Writer, set tipenya xlsx
        $writer = WriterFactory::create(Type::XLSX);
        // download to browser

        // set style untuk header
        $headerStyle = (new StyleBuilder())
               ->setFontBold()
               ->build();



    $writer->setTempFolder('tmp/'); //define temporary folder yg akan digunakan untuk menampung hasil file yg ditulis sementara
        $namaFile = 'Laporan_Detail_Penjualan_'.date('YmdHis').'.xlsx'; //nama filenya
        $filePath = 'tmp/' . $namaFile;

     $defaultStyle = (new StyleBuilder())
                ->setFontName('Arial')
                ->setFontSize(10)
                ->setShouldWrapText(false)
                ->build();
        $writer->setDefaultRowStyle($defaultStyle)
                ->openToFile($filePath);
             /*echo $this->db->last_query();
             echo "<hr>";*/
             $arrisi =array();
    $no=1;
    foreach($sql as $rk){
        $ff=array($no,$rk->kode_transaksi,$rk->tanggal_transaksi,$rk->nama_produk,$rk->satuan,$rk->qty,$rk->harga,$rk->jumlah);
        array_push($arrisi,$ff);
        $no++;
    }
    // write ke Sheet kedua
    $writer->getCurrentSheet()->setName('DetailPenjualan');
    // header Sheet kedua
    $writer->addRowWithStyle($header, $headerStyle);
    // data Sheet pertama
    $writer->addRows($arrisi);


     // close writter
        $writer->close();
        $this->load->helper('download');
        force_download($filePath, null);
    }
}