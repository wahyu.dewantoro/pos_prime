<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Pegawai extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Pegawai_model');
        $this->load->library('form_validation');
        $this->id_pengguna=get_userdata('app_id_pengguna');
    }

    private function cekAkses($var=null){
        $url='Pegawai';
        return cek($this->id_pengguna,$url,$var);
    }

    public function index()

    {
        $akses =$this->cekAkses('read');
        $q     = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));
        
        if ($q <> '') {
            $config['base_url']  = base_url() . 'pegawai?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'pegawai?q=' . urlencode($q);
        } else {
            $config['base_url']  = base_url() . 'pegawai';
            $config['first_url'] = base_url() . 'pegawai';
        }

        $config['per_page']          = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows']        = $this->Pegawai_model->total_rows($q);
        $pegawai                      = $this->Pegawai_model->get_limit_data($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'pegawai_data' => $pegawai,
            'q'                   => $q,
            'pagination'          => $this->pagination->create_links(),
            'total_rows'          => $config['total_rows'],
            'start'               => $start,
            'title'               => 'Data Pegawai',
            'create'              => 'Pegawai/create',
            'akses'               =>$akses
        );
        $this->template->load('layout','pegawai/Pegawai_list', $data);
    }

    

    public function create() 
    {
        $this->cekAkses('create');

        $data = array(
            'title'           =>'Tambah Data Pegawai',
            'kembali'         =>'Pegawai',
            'action'          => site_url('pegawai/create_action'),
            'nip'             => set_value('nip'),
            'nama'            => set_value('nama'),
            'hp'              => set_value('hp'),
            'tgl_mulai_kerja' => set_value('tgl_mulai_kerja'),
	);
        $this->template->load('layout','pegawai/Pegawai_form', $data);
    }
    
    public function create_action() 
    {
        $this->cekAkses('create');

        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
                'nama' => $this->input->post('nama',TRUE),
                'hp'   => $this->input->post('hp',TRUE),
        	    );
            // STR_TO_DATE('21-05-2013','%d-%m-%Y')
            $this->db->set('tgl_mulai_kerja',"STR_TO_DATE('".$this->input->post('tgl_mulai_kerja',TRUE)."','%d-%m-%Y')",false);
            $this->Pegawai_model->insert($data);
            set_flashdata('success', 'Data telah di simpan.');
            redirect(site_url('pegawai'));
        }
    }
    
    public function update($ide) 
    {
        $this->cekAkses('update');
        $id=rapikan($ide);
        $row = $this->Pegawai_model->get_by_id($id);

        if ($row) {
            $data = array(
                'title'           => 'Edit data Pegawai',
                'action'          => site_url('pegawai/update_action'),
                'kembali'         =>'Pegawai',
                'nip'             => set_value('nip', $row->nip),
                'nama'            => set_value('nama', $row->nama),
                'hp'              => set_value('hp', $row->hp),
                'tgl_mulai_kerja' => set_value('tgl_mulai_kerja',date('d-m-Y',strtotime( $row->tgl_mulai_kerja))),
	    );
            
            $this->template->load('layout','pegawai/Pegawai_form', $data);
        } else {
            set_flashdata('warning', 'Record Not Found.');
            redirect(site_url('pegawai'));
        }
    }
    
    public function update_action() 
    {
        $this->cekAkses('update');
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('nip', TRUE));
        } else {
            $data = array(
		'nama' => $this->input->post('nama',TRUE),
		'hp' => $this->input->post('hp',TRUE),
		// 'tgl_mulai_kerja' => $this->input->post('tgl_mulai_kerja',TRUE),
	    );
            $this->db->set('tgl_mulai_kerja',"STR_TO_DATE('".$this->input->post('tgl_mulai_kerja',TRUE)."','%d-%m-%Y')",false);
            $this->Pegawai_model->update($this->input->post('nip', TRUE), $data);
            set_flashdata('success', 'Update Record Success');
            redirect(site_url('pegawai'));
        }
    }
    
    public function delete($ide) 
    {
        $this->cekAkses('delete');
        $id=rapikan($ide);
        $row = $this->Pegawai_model->get_by_id($id);

        if ($row) {
            $this->Pegawai_model->delete($id);
            set_flashdata('success', 'Delete Record Success');
            redirect(site_url('pegawai'));
        } else {
            set_flashdata('message', 'Record Not Found');
            redirect(site_url('pegawai'));
        }
    }

    public function _rules() 
    {
	$this->form_validation->set_rules('nama', 'nama', 'trim|required');
	$this->form_validation->set_rules('hp', 'hp', 'trim|required');
	$this->form_validation->set_rules('tgl_mulai_kerja', 'tgl mulai kerja', 'trim|required');

	$this->form_validation->set_rules('nip', 'nip', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file Pegawai.php */
/* Location: ./application/controllers/Pegawai.php */