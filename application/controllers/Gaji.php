<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Gaji extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Gaji_model');
        $this->load->model('Pegawai_model');
        $this->load->library('form_validation');
        $this->id_pengguna=get_userdata('app_id_pengguna');
    }

    private function cekAkses($var=null){
        $url='Gaji';
        return cek($this->id_pengguna,$url,$var);
    }

    public function index()

    {
        $akses =$this->cekAkses('read');
        $q     = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));
        
        if ($q <> '') {
            $config['base_url']  = base_url() . 'gaji?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'gaji?q=' . urlencode($q);
        } else {
            $config['base_url']  = base_url() . 'gaji';
            $config['first_url'] = base_url() . 'gaji';
        }

        $config['per_page']          = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows']        = $this->Gaji_model->total_rows($q);
        $gaji                      = $this->Gaji_model->get_limit_data($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'gaji_data'  => $gaji,
            'q'          => $q,
            'pagination' => $this->pagination->create_links(),
            'total_rows' => $config['total_rows'],
            'start'      => $start,
            'title'      => 'Data Gaji',
            'create'     => 'Gaji/create',
            'akses'      =>$akses
        );
        $this->template->load('layout','gaji/Gaji_list', $data);
    }

    

    public function create() 
    {
        $this->cekAkses('create');

        $data = array(
            'title'        =>'Tambah Data Gaji',
            'kembali'      =>'Gaji',
            'action'       => site_url('gaji/create_action'),
            'id'           => set_value('id'),
            'nip'          => set_value('nip'),
            'nama_pegawai' => set_value('nama_pegawai'),
            'gaji_pokok'   => set_value('gaji_pokok'),
            'tunjangan'    => set_value('tunjangan'),
            'bonus'        => set_value('bonus'),
            'script'       =>'gaji/js_gaji',
            'pegawai'      =>$this->Pegawai_model->get_all()
	);
        $this->template->load('layout','gaji/Gaji_form', $data);
    }
    
    public function create_action() 
    {
        $this->cekAkses('create');

        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
                'nip'          => $this->input->post('nip',TRUE),
                'nama_pegawai' => $this->input->post('nama_pegawai',TRUE),
                'gaji_pokok'   => str_replace('.', '', $this->input->post('gaji_pokok',TRUE)),
                'tunjangan'    => str_replace('.', '', $this->input->post('tunjangan',TRUE)),
                'bonus'        => str_replace('.', '', $this->input->post('bonus',TRUE)),
    	    );

            $this->Gaji_model->insert($data);
            set_flashdata('success', 'Data telah di simpan.');
            redirect(site_url('gaji'));
        }
    }
    
    public function update($ide) 
    {
        $this->cekAkses('update');
        $id=rapikan($ide);
        $row = $this->Gaji_model->get_by_id($id);

        if ($row) {
            $data = array(
                'title'        => 'Edit data Gaji',
                'action'       => site_url('gaji/update_action'),
                'kembali'      =>'Gaji',
                'id'           => set_value('id', $row->id),
                'nip'          => set_value('nip', $row->nip),
                'nama_pegawai' => set_value('nama_pegawai', $row->nama_pegawai),
                'gaji_pokok'   => set_value('gaji_pokok', ribuan($row->gaji_pokok)),
                'tunjangan'    => set_value('tunjangan', ribuan($row->tunjangan)),
                'bonus'        => set_value('bonus', ribuan($row->bonus)),
	    );
            $this->template->load('layout','gaji/Gaji_form', $data);
        } else {
            set_flashdata('warning', 'Record Not Found.');
            redirect(site_url('gaji'));
        }
    }
    
    public function update_action() 
    {
        $this->cekAkses('update');
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id', TRUE));
        } else {
            $data = array(
                'nip'          => $this->input->post('nip',TRUE),
                'nama_pegawai' => $this->input->post('nama_pegawai',TRUE),
                'gaji_pokok'   => str_replace('.', '', $this->input->post('gaji_pokok',TRUE)),
                'tunjangan'    => str_replace('.', '', $this->input->post('tunjangan',TRUE)),
                'bonus'        => str_replace('.', '', $this->input->post('bonus',TRUE)),
	       );

            $this->Gaji_model->update($this->input->post('id', TRUE), $data);
            set_flashdata('success', 'Update Record Success');
            redirect(site_url('gaji'));
        }
    }
    
    public function delete($ide) 
    {
        $this->cekAkses('delete');
        $id=rapikan($ide);
        $row = $this->Gaji_model->get_by_id($id);

        if ($row) {
            $this->Gaji_model->delete($id);
            set_flashdata('success', 'Delete Record Success');
            redirect(site_url('gaji'));
        } else {
            set_flashdata('message', 'Record Not Found');
            redirect(site_url('gaji'));
        }
    }

    public function _rules() 
    {
	$this->form_validation->set_rules('nip', 'nip', 'trim|required');
	$this->form_validation->set_rules('nama_pegawai', 'nama pegawai', 'trim|required');
	$this->form_validation->set_rules('gaji_pokok', 'gaji pokok', 'trim|required');
	$this->form_validation->set_rules('tunjangan', 'tunjangan', 'trim|required');
	$this->form_validation->set_rules('bonus', 'bonus', 'trim|required');

	$this->form_validation->set_rules('id', 'id', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file Gaji.php */
/* Location: ./application/controllers/Gaji.php */