<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

    require_once APPPATH.'/third_party/spout/src/Spout/Autoloader/autoload.php';
    use Box\Spout\Writer\WriterFactory;
    use Box\Spout\Common\Type;
    use Box\Spout\Writer\Style\StyleBuilder;
class Lap_pembelian_per_supplier extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Pembelian_model');
        $this->load->model('Supplier_model');
        $this->load->model('Produk_model');
        $this->load->library('form_validation');
        $this->id_pengguna=get_userdata('app_id_pengguna');
        $this->username=get_userdata('app_username');
    }
    private function cekAkses($var=null){
        $url='Lap_pembelian_per_supplier';
        return cek($this->id_pengguna,$url,$var);
    }

    public function index()

    {
        $akses =$this->cekAkses('read');
        $pos_supplier_id=urldecode($this->input->get('pos_supplier_id',true));
        $start = intval($this->input->get('start'));
        if ($pos_supplier_id <> '') {
            $config['base_url']  = base_url() . 'lap_pembelian_per_supplier?pos_supplier_id='.urlencode($pos_supplier_id);
            $config['first_url'] = base_url() . 'lap_pembelian_per_supplier?pos_supplier_id='.urlencode($pos_supplier_id);
            $cetak= base_url() . 'lap_pembelian_per_supplier/cetak?pos_supplier_id='.urlencode($pos_supplier_id);
        } else {
            $config['base_url']  = base_url() . 'lap_pembelian_per_supplier';
            $config['first_url'] = base_url() . 'lap_pembelian_per_supplier';
            $cetak= base_url() . 'lap_pembelian_per_supplier/cetak';
        }


        $config['per_page']          = 10;
        $config['page_query_string'] = TRUE;
        if($pos_supplier_id!=''){
            $this->db->where("pos_suplier_id", $pos_supplier_id);
        }
        $config['total_rows']        = $this->Pembelian_model->laporan_total_rows();
        if($pos_supplier_id!=''){
            $this->db->where("pos_suplier_id", $pos_supplier_id);
        }
        $pembelian                        = $this->Pembelian_model->laporan_get_limit_data($config['per_page'], $start);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $this->db->order_by('nama', 'asc');
        $supplier_list = $this->Supplier_model->get_all();
        $data = array(
            'pembelian_data'      => $pembelian,
            'pagination'          => $this->pagination->create_links(),
            'total_rows'          => $config['total_rows'],
            'start'               => $start,
            'pos_supplier_id'     => $pos_supplier_id,
            'cetak'               => $cetak,
            'title'               => 'Laporan Pembelian per Supplier',
            'supplier_list'       => $supplier_list,
            'akses'               => $akses
        );
        $this->template->load('layout','lap_pembelian_per_supplier/lap_pembelian_per_supplier_list', $data);
    }
    function cetak()
    {
        ini_set('memory_limit', '-1');
        $pos_supplier_id=urldecode($this->input->get('pos_supplier_id',true));
        if($pos_supplier_id!=''){
            $this->db->where("pos_suplier_id", $pos_supplier_id);
        }
        $pembelian                        = $this->Pembelian_model->getAll_data();
        $header=['NO', 'Tanggal Pembelian', 'Nama Supplier', 'Nama Petugas', 'Kode Barang', 'Nama Barang','Jumlah', 'Harga', 'TOtal Pembelian'];
        // setup Spout Excel Writer, set tipenya xlsx
        $writer = WriterFactory::create(Type::XLSX);
        // download to browser

        // set style untuk header
        $headerStyle = (new StyleBuilder())
               ->setFontBold()
               ->build();
    $writer->setTempFolder('tmp/'); //define temporary folder yg akan digunakan untuk menampung hasil file yg ditulis sementara
        $namaFile = 'Laporan_Pembelian_per_supplier'.date('YmdHis').'.xlsx'; //nama filenya
        $filePath = 'tmp/' . $namaFile;

     $defaultStyle = (new StyleBuilder())
                ->setFontName('Arial')
                ->setFontSize(10)
                ->setShouldWrapText(false)
                ->build();
        $writer->setDefaultRowStyle($defaultStyle)
                ->openToFile($filePath);
             /*echo $this->db->last_query();
             echo "<hr>";*/
             $arrisi =array();
    $no=1;
    foreach($pembelian as $rk){
        $ff=array($no,date_indo($rk->tgl_pembelian),$rk->nama_suplier,$rk->nama_pegawai,$rk->kd_barang,$rk->nama_barang,$rk->qty_pembelian,$rk->harga_per_item,$rk->harga_jumlah);
        array_push($arrisi,$ff);
        $no++;
    }
    // write ke Sheet kedua
    $writer->getCurrentSheet()->setName('Pembelian');
    // header Sheet kedua
    $writer->addRowWithStyle($header, $headerStyle);
    // data Sheet pertama
    $writer->addRows($arrisi);


     // close writter
        $writer->close();
        $this->load->helper('download');
        force_download($filePath, null);
    }
}