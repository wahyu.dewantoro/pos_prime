<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');


    require_once APPPATH.'/third_party/spout/src/Spout/Autoloader/autoload.php';
    use Box\Spout\Writer\WriterFactory;
    use Box\Spout\Common\Type;
    use Box\Spout\Writer\Style\StyleBuilder;
class Lap_penggajian extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Penggajian_model');
        $this->load->model('Produk_model');
        $this->load->library('form_validation');
        $this->id_pengguna=get_userdata('app_id_pengguna');
        $this->username=get_userdata('app_username');
    }
    private function cekAkses($var=null){
        $url='Lap_penggajian';
        return cek($this->id_pengguna,$url,$var);
    }

    public function index()

    {
        $akses =$this->cekAkses('read');
        $bulan_gaji=urldecode($this->input->get('bulan_gaji',true));
        $tahun_gaji=urldecode($this->input->get('tahun_gaji',true));
        $start = intval($this->input->get('start'));
        if ($bulan_gaji <> '') {
            $config['base_url']  = base_url() . 'lap_penggajian?bulan_gaji='.urlencode($bulan_gaji).'&tahun_gaji='.urlencode($tahun_gaji);
            $config['first_url'] = base_url() . 'lap_penggajian?bulan_gaji='.urlencode($bulan_gaji).'&tahun_gaji='.urlencode($tahun_gaji);
            $cetak = base_url() . 'lap_penggajian/cetak?bulan_gaji='.urlencode($bulan_gaji).'&tahun_gaji='.urlencode($tahun_gaji);
        } else {
            $config['base_url']  = base_url() . 'lap_penggajian';
            $config['first_url'] = base_url() . 'lap_penggajian';
            $cetak = base_url() . 'lap_penggajian/cetak';
        }
        if($bulan_gaji==''){
            $bulan_gaji=date('m');
        }
        if($tahun_gaji==''){
            $tahun_gaji=date('Y');
        }
        $config['per_page']          = 10;
        $config['page_query_string'] = TRUE;
            $this->db->where("bulan_gaji", $bulan_gaji);
            $this->db->where("tahun_gaji", $tahun_gaji);
        $config['total_rows']        = $this->Penggajian_model->laporan_total_rows();
            $this->db->where("bulan_gaji", $bulan_gaji);
            $this->db->where("tahun_gaji", $tahun_gaji);
        $penggajian                        = $this->Penggajian_model->laporan_get_limit_data($config['per_page'], $start);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'penggajian_data'      => $penggajian,
            'pagination'          => $this->pagination->create_links(),
            'total_rows'          => $config['total_rows'],
            'start'               => $start,
            'bulan_gaji'          => $bulan_gaji,
            'tahun_gaji'          => $tahun_gaji,
            'cetak'                 => $cetak,
            'title'               => 'Laporan Penggajian',
            'akses'               => $akses
        );
        $this->template->load('layout','lap_penggajian/lap_penggajian_list', $data);
    }
    function cetak()
    {
        ini_set('memory_limit', '-1');
        $bulan_gaji=urldecode($this->input->get('bulan_gaji',true));
        $tahun_gaji=urldecode($this->input->get('tahun_gaji',true));
        $namabulan=array(
            '',
            'Januari',
            'Februari',
            'Maret',
            'April',
            'Mei',
            'Juni',
            'Juli',
            'Agustus',
            'September',
            'Oktober',
            'November',
            'Desember'
        );

        if($bulan_gaji==''){
            $bulan_gaji=date('m');
        }
        if($tahun_gaji==''){
            $tahun_gaji=date('Y');
        }
        $this->db->where("bulan_gaji", $bulan_gaji);
        $this->db->where("tahun_gaji", $tahun_gaji);
        $Penggajian                        = $this->Penggajian_model->getAll_data();
        $header=['NO', 'Tanggal Transaksi', 'Bulan Gaji', 'Tahun Gaji', 'NIP Pegawai', 'Nama Pegawai', 'Gaji Pokok', 'Tunjangan', 'Bonus'];
        // setup Spout Excel Writer, set tipenya xlsx
        $writer = WriterFactory::create(Type::XLSX);
        // download to browser

        // set style untuk header
        $headerStyle = (new StyleBuilder())
               ->setFontBold()
               ->build();



    $writer->setTempFolder('tmp/'); //define temporary folder yg akan digunakan untuk menampung hasil file yg ditulis sementara
        $namaFile = 'Laporan_Penggajian_'.date('YmdHis').'.xlsx'; //nama filenya
        $filePath = 'tmp/' . $namaFile;

     $defaultStyle = (new StyleBuilder())
                ->setFontName('Arial')
                ->setFontSize(10)
                ->setShouldWrapText(false)
                ->build();
        $writer->setDefaultRowStyle($defaultStyle)
                ->openToFile($filePath);
             /*echo $this->db->last_query();
             echo "<hr>";*/
             $arrisi =array();
    $no=1;
    foreach($Penggajian as $rk){
        $ff=array($no,date_indo($rk->tanggal_transaksi),$namabulan[$rk->bulan_gaji],$rk->tahun_gaji,$rk->nip_pegawai,$rk->nama_pegawai, $rk->gaji_pokok,$rk->tunjangan,$rk->bonus);
        array_push($arrisi,$ff);
        $no++;
    }
    // write ke Sheet kedua
    $writer->getCurrentSheet()->setName('Penggajian');
    // header Sheet kedua
    $writer->addRowWithStyle($header, $headerStyle);
    // data Sheet pertama
    $writer->addRows($arrisi);


     // close writter
        $writer->close();
        $this->load->helper('download');
        force_download($filePath, null);
    }
}