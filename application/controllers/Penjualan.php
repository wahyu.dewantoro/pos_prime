<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
    // require __DIR__ . '/vendor/autoload.php';
    use Mike42\Escpos\EscposImage;
    use Mike42\Escpos\PrintConnectors\FilePrintConnector;
    use Mike42\Escpos\Printer;
class Penjualan extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Penjualan_model');
        $this->load->model('Produk_model');
        $this->load->library('form_validation');
        $this->id_pengguna=get_userdata('app_id_pengguna');
        $this->nip=get_userdata('app_nip');
        $this->ip_printer=get_userdata('app_ip_printer');
    }

    private function cekAkses($var=null){
        $url='Penjualan';
        return cek($this->id_pengguna,$url,$var);
    }

    public function index()

    {
        $akses =$this->cekAkses('read');
        $q     = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));

        if ($q <> '') {
            $config['base_url']  = base_url() . 'penjualan?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'penjualan?q=' . urlencode($q);
        } else {
            $config['base_url']  = base_url() . 'penjualan';
            $config['first_url'] = base_url() . 'penjualan';
        }

        $config['per_page']          = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows']        = $this->Penjualan_model->total_rows($q);
        $penjualan                      = $this->Penjualan_model->get_limit_data($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'penjualan_data' => $penjualan,
            'q'              => $q,
            'pagination'     => $this->pagination->create_links(),
            'total_rows'     => $config['total_rows'],
            'start'          => $start,
            'title'          => 'Data Penjualan',
            'create'         => 'Penjualan/create',
            'akses'          =>$akses,
            'script'         =>'penjualan/js_penjualan_dua'
        );
        $this->template->load('layout2','penjualan/Penjualan_list', $data);
    }



    public function create()
    {
        $this->cekAkses('create');

        // delete pos_penjualan_item_temp
        $this->db->where('nip', $this->nip);
        $this->db->delete('pos_penjualan_item_temp');
        $data = array(
            'title'             =>'Tambah Data Penjualan',
            'kembali'           =>'Penjualan',
            'action'            => site_url('penjualan/create_action'),
            'id'                => set_value('id'),
            'nip'               => set_value('nip',get_userdata('app_nip')),
            'nama_pegawai'      => set_value('nama_pegawai',get_userdata('app_nama')),
            'pelanggan_id'      => set_value('pelanggan_id'),
            'nama_pelanggan'    => set_value('nama_pelanggan'),
            'tanggal_transaksi' => set_value('tanggal_transaksi'),
            'produk'            =>$this->Produk_model->get_all(),
            'script'            =>'penjualan/js_penjualan'
	);
        $this->template->load('layout2','penjualan/Penjualan_form', $data);
    }

    public function create_action()
    {
        $this->cekAkses('create');
        $this->_rules();





            if($this->input->post('nama_pelanggan')=='' ||$this->input->post('no_hp')=='' || empty($this->input->post('pos_produk_id',TRUE)) ){
                echo 0;
            }else{

            $this->db->trans_start();
            // delete pos_penjualan_item_temp

            $this->db->where('nip', $this->nip);
            $this->db->delete('pos_penjualan_item_temp');
            // cek pelanggan
            $this->db->where('nama_pelanggan',$this->input->post('nama_pelanggan'));
            $this->db->where('instansi',$this->input->post('instansi'));
            $this->db->where('email',$this->input->post('email'));
            $this->db->where('no_hp',$this->input->post('no_hp'));
            $this->db->select('id');
            $pelanggan=$this->db->get('pos_pelanggan')->row();
            if(empty($pelanggan)){
                // echo "string";
                $this->db->set('nama_pelanggan',$this->input->post('nama_pelanggan'));
                $this->db->set('instansi',$this->input->post('instansi'));
                $this->db->set('email',$this->input->post('email'));
                $this->db->set('no_hp',$this->input->post('no_hp'));
                $this->db->insert('pos_pelanggan');


                $this->db->where('nama_pelanggan',$this->input->post('nama_pelanggan'));
                $this->db->where('instansi',$this->input->post('instansi'));
                $this->db->where('email',$this->input->post('email'));
                $this->db->where('no_hp',$this->input->post('no_hp'));
                $this->db->select('id');
                $pelanggan_id=$this->db->get('pos_pelanggan')->row()->id;
            }else{
                $pelanggan_id=$pelanggan->id;
            }

            $data = array(
                'nip'            => $this->input->post('nip',TRUE),
                'nama_pegawai'   => $this->input->post('nama_pegawai',TRUE),
                'pelanggan_id'   => $pelanggan_id,
                'nama_pelanggan' => $this->input->post('nama_pelanggan',TRUE),
                'diskon'         => str_replace('.','',$this->input->post('diskon',true)),
                'jumlah_bayar'   => str_replace('.','',$this->input->post('jumlah_bayar',true)),
        	    );

            $this->db->set('tanggal_transaksi','now()',false);
            $this->db->insert('pos_penjualan',$data);
            $insert_id = $this->db->insert_id();
            $this->db->where($data);
            $this->db->select("max(id) id ");
            $pos_penjualan_id=$this->db->get('pos_penjualan')->row()->id;


            for($a=0;$a<count($this->input->post('pos_produk_id',TRUE));$a++){
                $pos_produk_id=$this->input->post('pos_produk_id',TRUE);
                if($pos_produk_id<>0){
                    $this->db->set('pos_penjualan_id',$pos_penjualan_id);
                    $this->db->set('nama_produk',$this->input->post('nama_produk')[$a]);
                    $this->db->set('pos_produk_id',$this->input->post('pos_produk_id')[$a]);
                    $this->db->set('satuan',$this->input->post('satuan')[$a]);
                    $this->db->set('qty',$this->input->post('qty')[$a]);
                    $this->db->set('harga',$this->input->post('harga')[$a]);
                    $this->db->set('jumlah',$this->input->post('jumlah')[$a]);
                    $this->db->insert('pos_penjualan_item');
                }
            }

        $this->db->trans_complete();

        if ($this->db->trans_status() === FALSE)
        {
            set_flashdata('warning', 'Data gagal di simpan.');
            echo 0;
        }else{
            set_flashdata('success', 'Data telah di simpan.');
            echo $insert_id;
            // redirect(site_url('penjualan/cetakstruk3/'.acak($pos_penjualan_id)));
        }

    }

        // redirect(site_url('penjualan'));

    }


    function  cetakstruk(){

        $id=(int)$this->input->get('data',true);
        $head=$this->db->query("SELECT a.*,jumlahtransaksi(id) jumlah FROM pos_penjualan a WHERE id=?",array($id))->row();
        $body=$this->db->query("SELECT * FROM pos_penjualan_item WHERE pos_penjualan_id=?",array($id))->result();
        $this->load->view('penjualan/cetakstrukv',array('head'=>$head,'body'=>$body));
    }

    function  cetakstruk2($ide){

        $id=(int)rapikan($ide);
        // $head=$this->db->query("SELECT a.*,jumlahtransaksi(id) jumlah FROM pos_penjualan a WHERE id=?",array($id))->row();
        // $body=$this->db->query("SELECT * FROM pos_penjualan_item WHERE pos_penjualan_id=?",array($id))->result();
        $data = array(
            'head' => $this->db->query("SELECT a.*,jumlahtransaksi(id) jumlah FROM pos_penjualan a WHERE id=?",array($id))->row() ,
            'body'  => $this->db->query("SELECT * FROM pos_penjualan_item WHERE pos_penjualan_id=?",array($id))->result()
         );
        // $this->load->view('penjualan/cetakstrukv2',array('head'=>$head,'body'=>$body));

		$html=$this->load->view('penjualan/cetakstrukv2',$data,true);
        $mpdf = new \Mpdf\Mpdf(['format' => [20, 40]]);
		$mpdf->WriteHTML($html);
		$mpdf->Output('yourFileName.pdf', 'F');
    }
    function columnify($leftCol, $rightCol, $leftWidth, $rightWidth, $space = 2)
    {
        $leftWrapped = wordwrap($leftCol, $leftWidth, "\n", true);
        $rightWrapped = wordwrap($rightCol, $rightWidth, "\n", true);

        $leftLines = explode("\n", $leftWrapped);
        $rightLines = explode("\n", $rightWrapped);
        $allLines = array();
        for ($i = 0; $i < max(count($leftLines), count($rightLines)); $i ++) {
            $leftPart = str_pad(isset($leftLines[$i]) ? $leftLines[$i] : "", $leftWidth, " ");
            $rightPart = str_pad(isset($rightLines[$i]) ? $rightLines[$i] : "", $rightWidth, " ", STR_PAD_LEFT);
            $allLines[] = $leftPart . str_repeat(" ", $space) . $rightPart;
        }
        // $right = str_pad($sign . $this -> price, $rightCols, ' ', STR_PAD_LEFT);
        return implode($allLines, "\n") . "\n";
    }
    function cetakstruk3($id)
    {
        // $this->db->where('id_inc', $this->id_pengguna);
        // $row=$this->db->get('ms_pengguna')->row();
        // if($row->ip_printer==null||$row->ip_printer=""){
        //     set_flashdata('warning', 'Setting IP Printer Dulu');
        //     redirect(site_url('penjualan'));
        // }
        // echo "//".$row->ip_printer."/POS-80";
        // $id=(int)rapikan($ide);
        $head=$this->db->query("SELECT a.*,jumlahtransaksi(id) jumlah FROM pos_penjualan a WHERE id=?",array($id))->row();
        $body=$this->db->query("SELECT * FROM pos_penjualan_item WHERE pos_penjualan_id=?",array($id))->result();

        // $tmpdir =  dirname(dirname(dirname(__FILE__)))."\upload";
        $file = $this->id_pengguna.'print.tmp';

        // echo $file;
        // die();
        /* Do some printing */
        $connector = new FilePrintConnector($file);
        $printer = new Printer($connector);

        $total = new item('Total', angka($head->jumlah_bayar));
        $diskon = new item('Diskon', angka($head->diskon));
        $sebelum = new item('Jumlah', angka($head->jumlah));
        /* Date is kept the same for testing */
        // $date = date('l jS \of F Y h:i:s A');

        /* Start the printer */
        $logo = EscposImage::load( dirname(dirname(dirname(__FILE__))) ."/assets/logo.png", false);
        $printer = new Printer($connector);

        /* Print top logo */
        $printer -> setJustification(Printer::JUSTIFY_CENTER);
        $printer -> bitImageColumnFormat($logo);


        /* Name of shop */
        $printer -> selectPrintMode();
        $printer -> text("PRIME Digital Printing.\n");
        $printer -> text("The best solutions for printing.\n\n");
        $printer -> setJustification(Printer::JUSTIFY_LEFT);
        $printer -> setTextSize(1, 8);
        $printer -> text("Tanggal : ".$head->tanggal_transaksi."\n");
        $printer -> text("Kode    : ".$head->kode_transaksi."\n");
        $printer -> text("Kasir   : ".$head->nama_pegawai."\n");
        $printer -> text("--------------------------------");
        $printer -> feed();

        foreach ($body as $rb) {
            $printer -> text($this->columnify($rb->nama_produk." x ".$rb->qty.' '.$rb->satuan, angka($rb->jumlah),22,8,2));
            // $printer -> text(new item($rb->nama_produk." x ".$rb->qty.' '.$rb->satuan, angka($rb->jumlah),TRUE));

            // $brg=$rb->nama_produk." x ".$rb->qty.' '.$rb->satuan;
            // $printer -> text(new item(wordwrap($brg,22,"\n"), angka($rb->jumlah)));
        }
        $printer -> text("--------------------------------\n");

        $printer -> text($sebelum);
        $printer -> text("--------------------------------\n");

        $printer -> text($diskon);
        $printer -> text("--------------------------------\n");
        $printer -> text($total);
        /* Footer */
        $printer -> feed(2);
        $printer -> setJustification(Printer::JUSTIFY_CENTER);
        $printer -> text("Terima Kasih\n");
        $printer -> text(date("Y-m-d H:i:s")."\n");
        $printer -> feed(2);
        // $printer -> text($date . "\n\n\n");

        /* Cut the receipt and open the cash drawer */
        $printer -> cut();
        $printer -> pulse();

        $printer -> close();
            // var_dump($file);
            // die();
        // copy($file, "//".$this->ip_printer."/POS-80");  # Lakukan cetak
        // unlink($file);
        echo $file;

    }
    public function hapusfile()
    {
        // copy($file, "//".$this->ip_printer."/POS-80");
        unlink($this->id_pengguna.'print.tmp');
        echo 'ok';

    }
    public function detail($ide)
    {
        $this->cekAkses('update');
        $id=rapikan($ide);
        $row = $this->Penjualan_model->get_by_id($id);

        if ($row) {

            $this->db->where('id',$row->pelanggan_id);
            $this->db->select('id,nama_pelanggan,instansi,email,no_hp');
            $pelanggan=$this->db->get('pos_pelanggan')->row();
            $head=$this->db->query("SELECT a.*,jumlahtransaksi(id) jumlah FROM pos_penjualan a WHERE id=?",array($id))->row();
            $body=$this->db->query("SELECT * FROM pos_penjualan_item WHERE pos_penjualan_id=?",array($id))->result();
            $data = array(
                'title' => 'Detail Data Penjualan',
                'action' => site_url('penjualan/update_action'),
                'kembali' =>'Penjualan',
                'id' => set_value('id', $row->id),
                'body'  =>$body,
                'head'  =>$head,
                'pelanggan'=>$pelanggan
	    );
            $this->template->load('layout','penjualan/Penjualan_detail', $data);
        } else {
            set_flashdata('warning', 'Record Not Found.');
            redirect(site_url('penjualan'));
        }
    }
    public function update($ide)
    {
        $this->cekAkses('update');
        $id=rapikan($ide);
        $row = $this->Penjualan_model->get_by_id($id);

        if ($row) {
            $data = array(
                'title' => 'Edit data Penjualan',
                'action' => site_url('penjualan/update_action'),
                'kembali' =>'Penjualan',
		'id' => set_value('id', $row->id),
		'nip' => set_value('nip', $row->nip),
		'nama_pegawai' => set_value('nama_pegawai', $row->nama_pegawai),
		'pelanggan_id' => set_value('pelanggan_id', $row->pelanggan_id),
		'nama_pelanggan' => set_value('nama_pelanggan', $row->nama_pelanggan),
		'tanggal_transaksi' => set_value('tanggal_transaksi', $row->tanggal_transaksi),
	    );
            $this->template->load('layout','penjualan/Penjualan_form', $data);
        } else {
            set_flashdata('warning', 'Record Not Found.');
            redirect(site_url('penjualan'));
        }
    }

    public function update_action()
    {
        $this->cekAkses('update');
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id', TRUE));
        } else {



            $data = array(
		'nip' => $this->input->post('nip',TRUE),
		'nama_pegawai' => $this->input->post('nama_pegawai',TRUE),
		'pelanggan_id' => $this->input->post('pelanggan_id',TRUE),
		'nama_pelanggan' => $this->input->post('nama_pelanggan',TRUE),
		'tanggal_transaksi' => $this->input->post('tanggal_transaksi',TRUE),
	    );

            $this->Penjualan_model->update($this->input->post('id', TRUE), $data);
            set_flashdata('success', 'Update Record Success');
            redirect(site_url('penjualan'));
        }
    }

    public function delete($ide)
    {
        $this->cekAkses('delete');
        $id=rapikan($ide);
        $row = $this->Penjualan_model->get_by_id($id);

        if ($row) {
            $this->Penjualan_model->delete($id);
            set_flashdata('success', 'Delete Record Success');
            redirect(site_url('penjualan'));
        } else {
            set_flashdata('message', 'Record Not Found');
            redirect(site_url('penjualan'));
        }
    }

    public function _rules()
    {
    	$this->form_validation->set_rules('nip', 'nip', 'trim|required');
    	$this->form_validation->set_rules('nama_pegawai', 'nama pegawai', 'trim|required');
    	// $this->form_validation->set_rules('pelanggan_id', 'pelanggan id', 'trim|required|numeric');
    	$this->form_validation->set_rules('nama_pelanggan', 'nama pelanggan', 'trim|required');
    	// $this->form_validation->set_rules('tanggal_transaksi', 'tanggal transaksi', 'trim|required');

    	$this->form_validation->set_rules('id', 'id', 'trim');
    	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

    function cekproduk()
    {
        $id  =(int)$this->input->post('produk',true);
        $qty =(int)$this->input->post('qty',true);
        $nip =$this->nip;
        $row = $this->db->query("SELECT COUNT(id) jml FROM pos_penjualan_item_temp WHERE nip=$nip and qty=$qty AND pos_produk_id=$id")->row();
        echo $row->jml;
    }

    function appendproduk(){
        $id  =(int)$this->input->post('produk',true);
        $qty =(int)$this->input->post('qty',true);
        $res=$this->Produk_model->get_by_id($id);
        $nip =$this->nip;
        $data = array(
            'nip' => $nip,
            'qty' => $qty,
            'pos_produk_id'=>$id
        );
        $this->db->insert('pos_penjualan_item_temp', $data);
        $insert_id = $this->db->insert_id();
        if($res){
            $html="<tr>
                        <td align='center'>".$qty."</td>
                        <td><b>".$res->nama_produk."</b><br>@".number_format($res->harga,0,'','.')."</td>
                        <td align='right'>".number_format($qty * $res->harga,0,'','.')." <a data-insert_id='".$insert_id."' data-jumlah='".$qty * $res->harga."' href='#' class='remove_project_file' tabindex='-1' ><i class=' fa fa-trash'></i></a></td>
                        <input type='hidden' name='pos_produk_id[]' value='".$res->id."' >
                <input type='hidden' name='nama_produk[]' value='".$res->nama_produk."' >
                <input type='hidden' name='satuan[]' value='".$res->satuan."' >
                <input type='hidden' name='qty[]' value='".$qty."' >
                <input type='hidden' name='harga[]' value='".$res->harga."' >
                <input type='hidden' name='jumlah[]' value='".$qty * $res->harga."' >
                   </tr>";


        /*$html="<tr >
                <td>".$res->nama_produk."</td>
                <td>".number_format($res->harga,0,'','.')."</td>
                <td>".$qty." ".$res->satuan ."</td>
                <td>".number_format($qty * $res->harga,0,'','.')."</td>
                <td><a data-jumlah='".$qty * $res->harga."' href='#' class='remove_project_file' ><i class=' fa fa-trash'></i></a></td>
                <input type='hidden' name='pos_produk_id[]' value='".$res->id."' >
                <input type='hidden' name='nama_produk[]' value='".$res->nama_produk."' >
                <input type='hidden' name='satuan[]' value='".$res->satuan."' >
                <input type='hidden' name='qty[]' value='".$qty."' >
                <input type='hidden' name='harga[]' value='".$res->harga."' >
                <input type='hidden' name='jumlah[]' value='".$qty * $res->harga."' >
            </tr>";*/
            echo $html;
        }
    }

    function tambahtotal(){
        // tambahtotal
        $id  =str_replace('.','',  $this->input->post('produk',true));
        $qty =str_replace('.','',  $this->input->post('qty',true));
        $total =str_replace('.','',  $this->input->post('total',true));
        $res=$this->Produk_model->get_by_id($id);
        if($res){
            echo number_format(($res->harga * $qty )+$total,0,'','.');
        }else{
            echo 0;
        }

    }

    function kurangtotal(){

        $this->db->where('id', $this->input->post('insert_id'));
        $this->db->delete('pos_penjualan_item_temp');
        $total=str_replace('.','',$this->input->post('total'));
        $kurang=str_replace('.','',$this->input->post('kurang'));
        echo number_format($total-$kurang,0,'','.');
    }
}

/* A wrapper to do organise item names & prices into columns */
class item
{
    private $name;
    private $price;
    private $dollarSign;

    public function __construct($name = '', $price = '', $dollarSign = false)
    {
        $this -> name = $name;
        $this -> price = $price;
        $this -> dollarSign = $dollarSign;
    }

    public function __toString()
    {
        $rightCols = 10;
        $leftCols = 22;
        if ($this -> dollarSign) {
            $leftCols = $leftCols / 2 - $rightCols / 2;
        }
        $left = str_pad($this -> name, $leftCols) ;

        $sign = ($this -> dollarSign ? 'Rp ' : '');
        $right = str_pad($sign . $this -> price, $rightCols, ' ', STR_PAD_LEFT);
        return "$left$right\n";
    }
}

/* End of file Penjualan.php */
/* Location: ./application/controllers/Penjualan.php */