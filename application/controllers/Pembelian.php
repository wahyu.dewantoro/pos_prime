<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Pembelian extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Pembelian_model');
        $this->load->model('Supplier_model');
        $this->load->model('Produk_model');
        $this->load->library('form_validation');
        $this->id_pengguna=get_userdata('app_id_pengguna');
        $this->username=get_userdata('app_username');
    }



    private function cekAkses($var=null){
        $url='pembelian';
        return cek($this->id_pengguna,$url,$var);
    }

    public function index()

    {
        $akses =$this->cekAkses('read');
        $q     = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));

        if ($q <> '') {
            $config['base_url']  = base_url() . 'pembelian?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'pembelian?q=' . urlencode($q);
        } else {
            $config['base_url']  = base_url() . 'pembelian';
            $config['first_url'] = base_url() . 'pembelian';
        }

        $config['per_page']          = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows']        = $this->Pembelian_model->total_rows($q);
        $pembelian                        = $this->Pembelian_model->get_limit_data($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'pembelian_data'       => $pembelian,
            'q'                   => $q,
            'pagination'          => $this->pagination->create_links(),
            'total_rows'          => $config['total_rows'],
            'start'               => $start,
            'title'               => 'Data Pembelian',
            'create'              => 'pembelian/create',
            'akses'               =>$akses
        );
        $this->template->load('layout','pembelian/pembelian_list', $data);
    }



    public function create()
    {
        $this->cekAkses('create');
        $this->db->order_by('nama', 'asc');
        $supplier_list = $this->Supplier_model->get_all();
        $data = array(
            'title'          =>'Tambah Data pembelian',
            'kembali'        =>'pembelian',
            'action'         => site_url('pembelian/create_action'),
            'id'             => set_value('id'),
            'tgl'            => set_value('tgl', date('Y-m-d')),
            'kd_barang'      => set_value('kd_barang'),
            'jumlah'         => set_value('jumlah'),
            'harga'          => set_value('harga'),
            'total'          => set_value('total'),
            'sup_id'         => set_value('sup_id'),
            'sup_nama'       => set_value('sup_nama'),
            'nama_barang'    => set_value('nama_barang'),
            'script'         =>'pembelian/js_pembelian',
            'supplier_list'  => $supplier_list,
            'produk'         => $this->Produk_model->get_all()
	);
        $this->template->load('layout','pembelian/pembelian_form', $data);
    }

    public function create_action()
    {
        $this->cekAkses('create');

        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
                    'tgl_pembelian'     => $this->input->post('tgl',TRUE),
                    'nip_pegawai'       => $this->id_pengguna,
                    'nama_pegawai'      => $this->username,
                    'pos_suplier_id'   => $this->input->post('supplier',TRUE),
                    'nama_suplier'     => $this->input->post('nama_supplier',TRUE),
                    'kd_barang'         => $this->input->post('kd_barang',TRUE),
                    'nama_barang'         => $this->input->post('nama_barang',TRUE),
                    'qty_pembelian'     => str_replace(".","",$this->input->post('jumlah',TRUE)),
                    'harga_per_item'    => str_replace(".","",$this->input->post('harga',TRUE)),
                    'harga_jumlah'      => str_replace(".","",$this->input->post('total',TRUE)),
            	    );

            $this->Pembelian_model->insert($data);
           set_flashdata('success', 'Data telah di simpan.');
            redirect(site_url('pembelian'));
        }
    }

    public function update($ide)
    {
        $this->cekAkses('update');
        $id=rapikan($ide);
        $row = $this->Pembelian_model->get_by_id($id);

        $this->db->order_by('nama', 'asc');
        $supplier_list = $this->Supplier_model->get_all();
        if ($row) {
            $data = array(
                'title'          => 'Edit Data pembelian',
                'action'         => site_url('pembelian/update_action'),
                'kembali'        => 'pembelian',
                'id'             => set_value('id', $row->id),
                'tgl'            => set_value('tgl', $row->tgl_pembelian),
                'kd_barang'      => set_value('kd_barang',$row->kd_barang),
                'nama_barang'    => set_value('nama_barang',$row->nama_barang),
                'jumlah'         => set_value('jumlah',number_format($row->qty_pembelian,0,",",".")),
                'harga'          => set_value('harga',number_format($row->harga_per_item,0,",",".")),
                'total'          => set_value('total',number_format($row->harga_jumlah,0,",",".")),
                'sup_id'         => set_value('sup_id',$row->pos_suplier_id),
                'sup_nama'       => set_value('sup_nama',$row->nama_suplier),
                'supplier_list'  => $supplier_list
	    );
            $this->template->load('layout','pembelian/pembelian_form', $data);
        } else {
           set_flashdata('warning', 'Record Not Found.');
            redirect(site_url('pembelian'));
        }
    }

    public function update_action()
    {
        $this->cekAkses('update');
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id_inc', TRUE));
        } else {
            $data = array(
                'tgl_pembelian'     => $this->input->post('tgl',TRUE),
                'nip_pegawai'       => $this->id_pengguna,
                'nama_pegawai'      => $this->username,
                'pos_suplier_id'   => $this->input->post('supplier',TRUE),
                'nama_suplier'     => $this->input->post('nama_supplier',TRUE),
                'kd_barang'         => $this->input->post('kd_barang',TRUE),
                'nama_barang'         => $this->input->post('nama_barang',TRUE),
                'qty_pembelian'     => str_replace(".","",$this->input->post('jumlah',TRUE)),
                'harga_per_item'    => str_replace(".","",$this->input->post('harga',TRUE)),
                'harga_jumlah'      => str_replace(".","",$this->input->post('total',TRUE)),
	    );
            $this->Pembelian_model->update($this->input->post('id', TRUE), $data);

            // echo $this->db->last_query();

           set_flashdata('success', 'Update Record Success');
            redirect(site_url('pembelian'));
        }
    }

    public function delete($ide)
    {
        $this->cekAkses('delete');
        $id=rapikan($ide);
        $row = $this->Pembelian_model->get_by_id($id);

        if ($row) {
            $this->Pembelian_model->delete($id);
           set_flashdata('success', 'Delete Record Success');
            redirect(site_url('pembelian'));
        } else {
           set_flashdata('message', 'Record Not Found');
            redirect(site_url('pembelian'));
        }
    }

    public function _rules()
    {
	$this->form_validation->set_rules('tgl', 'Tanggal Pembelian', 'trim|required');
	$this->form_validation->set_rules('jumlah', 'jumlah', 'trim|required|numeric');
	$this->form_validation->set_rules('harga', 'harga', 'trim|required|numeric');
	$this->form_validation->set_rules('supplier', 'supplier', 'trim|required');
	$this->form_validation->set_rules('kd_barang', 'kode barang', 'trim|required');

	$this->form_validation->set_rules('id', 'id', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file Menu.php */
/* Location: ./application/controllers/Menu.php */