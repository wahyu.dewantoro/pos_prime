<?php 

/*$string = "<!doctype html>
<html>
    <head>
        <title>harviacode.com - codeigniter crud generator</title>
        <link rel=\"stylesheet\" href=\"<?php echo base_url('assets/bootstrap/css/bootstrap.min.css') ?>\"/>
        <style>
            body{
                padding: 15px;
            }
        </style>
    </head>
    <body>but
        <h2 style=\"margin-top:0px\">".ucfirst($table_name)." <?php echo \$button ?></h2>
        <form action=\"<?php echo \$action; ?>\" method=\"post\">";
foreach ($non_pk as $row) {
    if ($row["data_type"] == 'text')
    {
    $string .= "\n\t    <div class=\"form-group\">
            <label for=\"".$row["column_name"]."\">".label($row["column_name"])." <?php echo form_error('".$row["column_name"]."') ?></label>
            <textarea class=\"form-control\" rows=\"3\" name=\"".$row["column_name"]."\" id=\"".$row["column_name"]."\" placeholder=\"".label($row["column_name"])."\"><?php echo $".$row["column_name"]."; ?></textarea>
        </div>";
    } else
    {
    $string .= "\n\t    <div class=\"form-group\">
            <label for=\"".$row["data_type"]."\">".label($row["column_name"])." <?php echo form_error('".$row["column_name"]."') ?></label>
            <input type=\"text\" class=\"form-control\" name=\"".$row["column_name"]."\" id=\"".$row["column_name"]."\" placeholder=\"".label($row["column_name"])."\" value=\"<?php echo $".$row["column_name"]."; ?>\" />
        </div>";
    }
}
$string .= "\n\t    <input type=\"hidden\" name=\"".$pk."\" value=\"<?php echo $".$pk."; ?>\" /> ";
$string .= "\n\t    <button type=\"submit\" class=\"btn btn-primary\"><?php echo \$button ?></button> ";
$string .= "\n\t    <a href=\"<?php echo site_url('".$c_url."') ?>\" class=\"btn btn-default\">Cancel</a>";
$string .= "\n\t</form>
    </body>
</html>";*/

$string="<div class=\"row\">
            <div class=\"col-12\">
                <div class=\"card-box\">
                    <form action=\"<?php echo \$action; ?>\" method=\"post\"  class=\"form-horizontal\" role=\"form\">";
                   foreach ($non_pk as $row) {
                    if ($row["data_type"] == 'text')
                    {
                    $string .= "\n\t    
                        <div class=\"form-group row\">
                            <label for=\"".$row["column_name"]."\" class=\"col-3 col-form-label\">".label($row["column_name"])." <sup style=\"color:red;\">*</sup></label>
                            <div class=\"col-9\">
                                <textarea class=\"form-control\" rows=\"3\" name=\"".$row["column_name"]."\" id=\"".$row["column_name"]."\" placeholder=\"".label($row["column_name"])."\"><?php echo $".$row["column_name"]."; ?></textarea>
                                <?php echo form_error('".$row["column_name"]."') ?>                                
                            </div>
                        </div>";
                    } else
                    {
                    $string .= "\n\t    
                        <div class=\"form-group row\">
                            <label for=\"".$row["column_name"]."\" class=\"col-3 col-form-label\">".label($row["column_name"])." <sup style=\"color:red;\">*</sup></label>
                            <div class=\"col-9\">
                                <input type=\"text\" class=\"form-control\"  name=\"".$row["column_name"]."\" id=\"".$row["column_name"]."\" placeholder=\"".label($row["column_name"])."\" value=\"<?php echo $".$row["column_name"]."; ?>\">
                                <?php echo form_error('".$row["column_name"]."') ?>                                
                            </div>
                        </div>";
                    }
                }     
                $string .= "\n\t    <input type=\"hidden\" name=\"".$pk."\" value=\"<?php echo $".$pk."; ?>\" /> ";
                  $string.="\n\t<div class=\"form-group mb-0 justify-content-end row\">
                            <div class=\"col-9\">
                                <button type=\"submit\" class=\"btn btn-success waves-effect waves-light\"><i class=\"fa fa-save\"></i> Submit</button>
                                <button type=\"reset\" class=\"btn btn-warning waves-effect waves-light\"><i class=\"fa fa-refresh\"></i> Reset</button>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>";

$hasil_view_form = createFile($string, $target."views/" . $c_url . "/" . $v_form_file);

?>